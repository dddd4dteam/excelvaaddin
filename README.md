 # ExcelVAAddIn

Excel Addin - Virtual Analyser Addin ( for Excel 2016 ver)
  Virtual Analyser Addin helps to do some Processing/analyze of  Excel Files
### 1 Task Data Copying 
    copy data from several excel files(source files) tp one target(like otchet) excel file.
       
### 2 Task anlysing/checking - different cell/ range checks for somthing. Here we have two file types- its Two files pattern . 
       
	    1-Original -Template Excel Form file to fill it 
	    and
	    
	    2 Target/Received file - already filled [1] file - 
			HERE we checkin - Headers, Formulas, Data by our Cuatom CHECK-FUNCS 
			     we finding errors and coorecting data.

### Technical Features
  - This Adding is based on Singleton Pattern
  - Use Excel.interop excel-api , 
  - use infragistics excel-api to load chage excel files in c# without using Ecel.App COM Interop objects.
  - use simple  own c# native excel cells model to give us   possibility to switch berween different excel-api libs, 
  
  -  All UI Bilded on  WPF 
        -with using MVVM- UI composition Pattern
        -with own UI Manager to show/navigate between views/windows.
 -  Processing has its class model - to develop disfferent processing tasks,  to group tasks, and indicates when tasks will br in processing state.
 - Use MEF to collect different  custom Processsing items- checks
 - Collect error results after processing  and help to remove this mistakes quickly throw the UI. 
- Processomg Model is configuration that can be saved and load from json file. 
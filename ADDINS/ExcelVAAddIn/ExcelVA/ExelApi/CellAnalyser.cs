﻿using Excel = Microsoft.Office.Interop.Excel;



namespace ExcelVA.XL
{

    #region --------CELL ANALYSER TASKS-----------

    // [group 1] - MERGED CELLS INFO
    // Check Cell IS MERGED
    // Get MERGED-Cell Value
    // Get MERGED-Cell Range
    
    //  [group 2] - CELLS ANALISING
    // LC -[Last Cell]
    // Get Last Cell.Row                GetLCRow            
    // Get Last Cell.Column             GetLCColumn         
    // Get Last Cell.Row in Column      GetLCRowInColumn    
    // Get Last Cell.Column in Row      GetLCColumnInRow    
    

   
    #endregion -------- CELL ANALYSER TASKS -----------



    public class CellAnalyser
    {

        #region -------- [group 1] - MERGED CELLS INFOG-----------

        #endregion -------- [group 1] - MERGED CELLS INFO ---------


        #region -------- [group 2] - CELLS ANALISING-----------

        /// <summary>
        /// Get [Last Cell].Row
        /// </summary>
        /// <returns></returns>
        public static long GetLCRow()
        {
            Excel.Worksheet sht = ExcelApp.App.ActiveSheet as Excel.Worksheet;
            return sht.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
        }

        /// <summary>
        /// Get [Last Cell].Column
        /// </summary>
        /// <returns></returns>
        public static long GetLCColumn()
        {
            Excel.Worksheet sht = ExcelApp.App.ActiveSheet as Excel.Worksheet;
            return sht.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Column;
        }

        /// <summary>
        /// Get [Last Cell].Row in Column
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        public static long GetLCRowInColumn(int columnIndex)
        {
            Excel.Worksheet sht = ExcelApp.App.ActiveSheet as Excel.Worksheet;

            return 0;
        }


        /// <summary>
        /// Get [Last Cell].Column in Row
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        public static long GetLCColumnInRow(int rowIndex)
        {
            Excel.Worksheet sht = ExcelApp.App.ActiveSheet as Excel.Worksheet;

            return 0;
        }
        #endregion -------- [group 2] - CELLS ANALISING-----------
        
         



    }
}

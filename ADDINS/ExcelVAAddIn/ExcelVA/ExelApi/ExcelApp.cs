﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;


namespace ExcelVA.XL
{
    public class ExcelApp
    {
        //Microsoft.Office.Interop.Excel.Range 
        // sdsSelectedRange = Globals.ThisAddIn.Application.Selection;

        #region ---------- EXCEL MAIN :APP/WB/SHEET ------------

        /// <summary>
        /// Main App - Visible App.
        /// </summary>
        public static Excel.Application App
        { get; private set; }

        /// <summary>
        /// Excel Main - Visible Window
        /// </summary>
        public static Excel.Window MainWindow
        { get; private set; }

        /// <summary>
        /// Excel Main Workbook- Visible Workbook
        /// </summary>
        public static Excel.Workbook MainBook
        { get; private set; }

        /// <summary>
        /// Excel Main Worksheet- Visible Worksheet
        /// </summary>
        public static Excel.Worksheet MainSheet
        { get; private set; }

        #endregion---------- EXCEL MAIN :APP/WB/SHEET ------------


        #region --------- EXCEL BACK :APP/WB/SHEET ------------


        /// <summary>
        /// Background App for invisible workbooks reading /writing
        /// </summary>
        public static Excel.Application BackApp
        { get; private set; }

        /// <summary>
        /// Excel Main Workbook- Visible Workbook
        /// </summary>
        public static Excel.Workbook BackBook
        { get; private set; }

        /// <summary>
        /// Excel Main Worksheet- Visible Worksheet
        /// </summary>
        public static Excel.Worksheet BackSheet
        { get; private set; }

        #endregion --------- EXCEL BACK :APP/WB/SHEET ------------


        #region ------------ EXCEL FILES ---------------

        public static List<string> ExcelExtensions =
          new List<string>() { ".xlsx", ".xlsb" };

        public static bool IsExcelFile(string fileExt)
        {
            return ExcelExtensions.Contains(fileExt);
        }


        public const string ExcelFileFilter = "Excel files (*.xlsx)|*.xlsx|"
                                    + "Excel files (*.xls)|*.xls|"
                                    + "Excel files (*.xlsb)|*.xlsb|"
                                    + "Excel files (*.xslt)|*.xslt";


        #endregion ------------ EXCEL FILES -------------



        public static void NullifyExcelApp()
        {

            App = null;
            MainBook = null;
            MainSheet = null;
            BackApp = null;
            BackBook = null;
            BackSheet = null;

        }
        
        public static void SetExcelApp(Excel.Application app)
        {  App = app;  }

        public static void SetBackApp(Excel.Application app)
        {  
            BackApp = app;
        }

        public static void LoadMainWB(string filePath)
        {
            MainBook = App.Workbooks.Open(filePath);
        }

        public static void LoadBackWB(string filePath)
        {
            BackApp = new Excel.Application { Visible = false };
            SelectBackWB(filePath); 
        }



        public static void SelectMainWB(string wBookKey)
        {
            MainBook = App.Workbooks[wBookKey];
        }
        public static void SelectBackWB(string wbFilePath)
        {
            BackBook = BackApp.Workbooks.Open(wbFilePath);
        }

        public static void SelectMainSheet(string sheetKey)
        {
            MainSheet = MainBook.Sheets[sheetKey] as Excel.Worksheet;
        }
                 
        public static void SelectBackSheet(string sheetKey)
        {
            BackSheet = BackBook.Sheets[sheetKey] as Excel.Worksheet;
        }


        public static string GetMainSheetCellVal(int col, int row)
        {            
            return  MainSheet.Cells.get_Item(row,col).S();             
        }
        public static void SetMainSheetCellVal(int col, int row, string value)
        {
            ExcelApp.MainSheet.Cells.set_Item(row, col, value); 
        }
        public static string GetBackSheetCellVal(int col, int row)
        {
            return BackSheet.Cells.get_Item(row, col).S();
        } 
        public static void SetBackSheetCellVal(int col, int row, string value)
        { 
             ExcelApp.BackSheet.Cells.set_Item(row, col, value); 
            //Excel.Range excelRange = BackSheet.Cells UsedRange;
            //excelRange.Cells.set_Item(row, col, value);
        }
        public static void SetCellVal(int col, int row,string value)
        {
            //  return AnalyseSheet.Cells[row, col].Value = value;// ' .S();
            Excel.Range excelRange = MainSheet.UsedRange;
            excelRange.Cells.set_Item(row, col, value);
        }


        /*
        int[] intArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        Range rng = excelApp.get_Range("A1", "J1");
        rng.Value = intArray; 
        */

        /*
        var abc = worksheet.Cells[2, 1].Value;
        Range row1 = worksheet.Rows.Cells[1, 1];
        Range row2 = worksheet.Rows.Cells[2, 1];

        row1.Value = "Test100";
        row2.Value = "Test200";
        */


       

        #region ---------- CELL COLORS -------------

        public static void SetActShtCellColor(int colmn, int row
                                   , Excel.XlRgbColor color)
        {
            var actWorkSheet = (App.ActiveSheet as Excel.Worksheet);
            if (actWorkSheet == null) return;

            var cellRange = actWorkSheet.Cells[row, colmn] as Excel.Range;
            if (cellRange == null) return;
            cellRange.Interior.Color = color;

        }

        public static void SetAnlsShtCellColor(int colmn, int row
                                    , Excel.XlRgbColor color)
        {
            if (MainSheet == null) return;

            var cellRange = MainSheet.Cells[row, colmn] as Excel.Range;
            if (cellRange == null) return;
            cellRange.Interior.Color = color;
        }

        #endregion ---------- CELL COLORS -------------


    }
}

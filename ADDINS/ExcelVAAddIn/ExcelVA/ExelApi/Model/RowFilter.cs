﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;

namespace ExcelVA.XL.Model
{

    /// <summary>
    /// Filter here is additional Column value  that can exist in both in Source and Target Sheets. 
    /// Task: From  What Source row/cell in data Column 
    ///        To where  Target row/cell in data Column 
    ///        we will copy data, 
    ///        if value in filter columns equals for theese rows(Source and Target)
    /// </summary>
    public class RowFilter: INotifyPropertyChanged
    {

        #region ----------- INotifyPropertyChanged ---------------

        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Raise Property changed event handler.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        public virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }


        /// <summary>
        ///Raise Property changed event handler.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(String propertyName)
        {
            //var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion ----------- INotifyPropertyChanged ---------------


        #region ------------------NOTIFIABLE MODEL ITEM:SourceFilterColumn ------------------------------
 
        Cell _SourceFilterColumnCell  =  Cell.FirstCell;

        /// <summary>
        ///   Source Filter Column  by Cell value
        /// </summary>
        public Cell SourceFilterColumnCell 
        {
            get { return _SourceFilterColumnCell; }
            set
            {
                _SourceFilterColumnCell  = value; //Field changing
                OnPropertyChanged(() => SourceFilterColumnCell);
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: SourceFilterColumn ------------------------------

        #region ------------------NOTIFIABLE MODEL ITEM: TargetFilterColumnCell--------------------------

        Cell _TargetFilterColumnCell =  Cell.FirstCell;

        /// <summary>
        /// Description TargetFilterColumnCell
        /// </summary>
        public Cell TargetFilterColumnCell
        {
            get { return _TargetFilterColumnCell; }
            set
            {
                _TargetFilterColumnCell = value; //Field changing
                OnPropertyChanged(() => TargetFilterColumnCell);
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: ItemName------------------------------


        #region ------------------NOTIFIABLE MODEL ITEM: FilterValue---------------------------
 
        string  _FilterValue = "";

        /// <summary>
        /// Description  FilterValue
        /// </summary>
        public string  FilterValue
        {
            get { return _FilterValue; }
            set
            {
                _FilterValue = value; //Field changing
                OnPropertyChanged(() => FilterValue);
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: FilterValue------------------------------
         

    }
}

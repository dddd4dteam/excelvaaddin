﻿using System;
using System.Collections.Generic;

namespace ExcelVA.XL.Model
{


    public class Cell : IEquatable, IEquatable<Cell>
    {
        public Cell() { }
        public Cell(string address)
        {
            Adres = new Address(address);
        }
        public Cell( int row , string col , string sheet)
        {
            var adres = sheet + "!" + col + row.S(); 
            Adres = new Address(adres);
        }
        public static Cell FirstCell 
        { get; } = new Cell("A1");  
       
        public Address Adres
        { get; set; } 
        public string Text
        { get; set; }


           //MERGED,COLOR,BORDER ...  
        public Dictionary<string, string> Format
        { get; set; } = new Dictionary<string, string>();



        public Cell NextInCol()
        {
            return new Cell(
                Adres.Column.Val + (Adres.Row++).S()
                );
        }

        public Cell NextInRow()
        {
            return new Cell(
                Adres.Column.Next().Val + Adres.RowFmt
                );
        }


        public Cell PrevInCol()
        {
            return new Cell(
                Adres.Column.Val + (Adres.Row--).S()
                );
        }

        public Cell PrevInRow()
        {
            return new Cell(
                Adres.Column.Prev().Val + Adres.RowFmt
                );
        }



        public void GetValueIG()
        {

        }


        public void SetBackSheetCellVal()
        {
           ExcelApp.SetBackSheetCellVal(Adres.Column.Index, Adres.Row, Text);
        }
        public void GetBackSheetCellVal()
        {
          Text = ExcelApp.GetBackSheetCellVal(Adres.Column.Index, Adres.Row);
        }

        public void GetMainSheetCellVal()
        {
          Text = ExcelApp.GetMainSheetCellVal(Adres.Column.Index, Adres.Row);
        }
        public void SetMainSheetCellVal()
        {
            ExcelApp.SetMainSheetCellVal(Adres.Column.Index, Adres.Row, Text);
        }

        public bool Equals(Cell otherCell)
        {
            //COMPARE ADDRESS
            if (Adres.Equals(otherCell.Adres) == false)
            {
                return false;
            }

            if (Text != otherCell.Text)
            {
                return false;
            }

            foreach (var item in Format)
            {
                if ( !otherCell.Format.ContainsKey(item.Key)
                    || otherCell.Format[item.Key] != item.Value
                   )
                {
                    return false;
                }
            }

            return true;
        }


        public bool EqualsTo(object other)
        {
            return Equals(other as Cell);
        }


    }


}


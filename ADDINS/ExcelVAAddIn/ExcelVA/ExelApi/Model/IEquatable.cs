﻿namespace System
{
    public interface IEquatable
    {
        bool EqualsTo(object otherItem);
    }
}

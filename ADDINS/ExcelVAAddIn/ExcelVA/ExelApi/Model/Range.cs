﻿
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.Windows.Documents;

namespace ExcelVA.XL.Model
{ 

    public class Range : IEquatable, IEquatable<Range>
    {
        public Range() { }

        public Range(string rangeAddres)
        {
            Address = rangeAddres;
            ParseAddress();
        }


        public static Range  RangeInColumn(string sheet , XCol colOfRange ,int startRow,int endRow)
        {
           var  addresStr = sheet + Symbols.ShDm
                    + colOfRange.Val
                    + startRow.S()
                    + Symbols.RgDm
                    + sheet + Symbols.ShDm
                    + colOfRange.Val
                    + endRow.S();
           return new Range(addresStr);

        }

        public static Range RangeInColumn( Cell sheetCell, Cell startColCell , Cell endColCell)
        {
          var addresStr = sheetCell.Adres.Sheet + Symbols.ShDm
                     + startColCell.Adres.Column.Val
                     + startColCell.Adres.RowFmt
                     + Symbols.RgDm
                     + sheetCell.Adres.Sheet + Symbols.ShDm
                     + endColCell.Adres.Column.Val
                     + endColCell.Adres.RowFmt;
            return new Range(addresStr);
        }



        [JsonIgnore]
        public static Range FirstRange
        { get; } = new Range("A1:B1");
       
        public string Address
        { get; set; }

        public Cell From
        { get; set; }
                 
        public Cell To
        { get; set; }


        #region -------- IEquatable ------------


        /// <summary>
        ///  Is this Range equals to other range
        /// </summary>
        /// <param name="otherRange"></param>
        /// <returns></returns>
        public bool Equals(Range otherRange)
        {
            if (Address != otherRange.Address)
            {
                return false;
            }

            if (From.Equals(otherRange.From) == false)
            {
                return false;
            }

            if (To.Equals(otherRange.To) == false)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        ///  Is this Range equals to other range object
        /// </summary>
        /// <param name="otherRange"></param>
        /// <returns></returns>
        public bool EqualsTo(object otherRange)
        {
            return Equals(otherRange as Range);
        }


        #endregion -------- IEquatable ------------

        /// <summary>
        /// Parse Range Address or throw InvalidOperationException
        /// </summary>
        public void ParseAddress()
        {
            //CELL FROM   CELL TO            
            // Exmpl: [D8:G20] 

            if (Address.IsNotNull())
            {
                var cells = Address.SplitNoEntries(Symbols.RgDm);
                if (cells.Count == 2)
                {
                    From = new Cell(cells[0]);
                    To = new Cell(cells[1]);
                }
                else 
                    throw new InvalidOperationException(
                    " Incorrect adress for Range parse"
                    );
            }
        }



        public List<Cell> CollectCells()
        {
            var result = new List<Cell>();
            for (int i = From.Adres.Column.Index; 
                        i <= To.Adres.Column.Index; i++)
            {
                for (int j = From.Adres.Row; j <= To.Adres.Row; j++)
                {
                    result.Add( new Cell(j,  new XCol(i).Val, From.Adres.Sheet) );
                } 
            }

            return result; 
        }
    }
}

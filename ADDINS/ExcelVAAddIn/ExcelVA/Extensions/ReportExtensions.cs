﻿using ExcelVA.Processing;
using ExcelVA.UI;
using System;
using System.Linq;

namespace ExcelVA
{
    public static class ReportExtensions
    {


        public static void TotProgr(this ProgressReporter progressReporter, string message, double percent)
        {
            progressReporter.Report(
                                ProgressInfo.NewTotalProgress(message, percent));
        }


        public static void ItmProgr(this ProgressReporter progressReporter,int index, string message, double percent)
        {
            progressReporter.Report(
                                ProgressInfo.NewItemProgress(index,message, percent));
        }


        public static void ErrProgr(this ProgressReporter progressReporter, string message)
        {
            progressReporter.Report(
                                ProgressInfo.NewError(message));
        }

         

    }
}

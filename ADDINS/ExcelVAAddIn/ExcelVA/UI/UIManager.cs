﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;
using WND = System.Windows;
using WPF = System.Windows.Controls;



namespace ExcelVA.UI
{

    public enum ShowTypeEn
    {
        /// <summary>
        /// Show all Views in one Main-Host Window. 
        /// Standart Navigation/Paging sheme
        /// </summary>
         Show 
       ,
        /// <summary>
        /// Show  each Views  in its own Host Window. 
        /// </summary>
        ShowDialog
    }
    internal class NavInfo
    {
       public static NavInfo NewTrack( Type viewType
                                , ShowTypeEn showType
                                ,bool caching
                                ,params KeyValuePair<string, object>[] viewParameters)
        {
            return new NavInfo()
            { ViewID = GetViewID(viewType)
            , ViewType= viewType
            , ShowType= showType
            , Caching = caching
            , Parameters = viewParameters
            }; 

        }
        public int ViewID
        { get; set; }
        public Type ViewType
        { get; set; }
        public ShowTypeEn ShowType
        { get; set; } = ShowTypeEn.Show;

        public bool Caching
        { get; set; }
        public KeyValuePair<string, object>[] Parameters
        { get; set; }

        static int GetViewID(Type viewType)
        {
            return viewType.FullName.GetHashCode();
        }


    }


    /// <summary>
    /// UI Manager manages by Views and Windows - instantiating an  caching objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UIManager<T>:IDisposable
       where T : WND.Window, IHostWindow, new()
    {

        #region ------------------ PROPERTIES  --------------------
        IHostWindow CacheMainWindow
        { get; set; }
        
        Dictionary<int, WPF.UserControl> CacheViews
        { get; set; } = new Dictionary<int, WPF.UserControl>();

        /// <summary>
        /// Maximum Count of Cached Views 
        /// </summary>
        public int MAX_Views
        { get; set; } = 5;

        Dictionary<int, IHostWindow> CacheDialogs
        { get; set; } = new Dictionary<int, IHostWindow>();

        /// <summary>
        /// Maximum Count of Cached Dialogs
        /// </summary>
        public int MAX_Dialogs
        { get; set; } = 5;


        #endregion ------------------ PROPERTIES  --------------------

        #region --------------- NAV TRACKER ------------------

        Stack<NavInfo> NavTracker
        { get; set; } = new Stack<NavInfo>();

        void SaveTrack(Type viewType
                    , bool caching
                    , ShowTypeEn showType
                    , params KeyValuePair<string, object>[] viewParameters)
        {
            var newTrack = NavInfo.NewTrack( viewType
                                ,showType,caching, viewParameters );
            NavTracker.Push(newTrack);
        }

        void ShowPreviosTrack()
        {
            NavTracker.Pop();// pop closed Window

            if (NavTracker.Count > 0)
            {
                var lasttrack = NavTracker.Peek();
                if ( lasttrack.ShowType == ShowTypeEn.Show )
                {                     
                    Show(lasttrack.ViewType, lasttrack.Caching 
                        ,isBackTrack: true 
                        , lasttrack.Parameters);
                }
                else if (lasttrack.ShowType == ShowTypeEn.ShowDialog)
                {
                    ShowDialog(lasttrack.ViewType, lasttrack.Caching
                        , isBackTrack: true
                        , lasttrack.Parameters);
                }
            }
           
        }


        #endregion --------------- NAV TRACKER ------------------


        public void Dispose()
        {
            CacheMainWindow = null;
            CacheDialogs.Clear();
            CacheViews.Clear();
        }

        static int GetViewID<TView>()
        {
            return typeof(TView).FullName.GetHashCode();
        }
        static int GetViewID(Type viewType)
        {
            return viewType.FullName.GetHashCode();
        }



        #region ------------ TAB ITEMS ---------------

        public void SetTabItem<TView>(string tabItemKey, TabControl tabElement)
         where TView : UserControl, IView, new()
        {
            var targetView = GetViewWithCache<TView>(cacheView: true);
            var item = tabElement.FindName(tabItemKey) as TabItem;
            item.Content = targetView;
        }

        public void SetTabItem(TabItem tabItemElement, Type viewType)

        {
            var tabItem = tabItemElement;
            var targetView = GetViewWithCache(viewType);

            tabItem.Content = targetView;
        }

        public void SetTabItem(string tabItemKey, TabControl tabElement, Type viewType)
        {
            var targetView = GetViewWithCache(viewType, cacheView: true);
            var item = tabElement.FindName(tabItemKey) as TabItem;
            item.Content = targetView;
        }


        #endregion ------------ TAB ITEMS ---------------


        #region ---------- SHOW  SHOWDIALOG------------
        void Show(Type viewType
                  , bool cacheView = false
                  , bool isBackTrack = false
                  , params KeyValuePair<string, object>[] viewParameters
                 )
        {
            var viewID = GetViewID(viewType);
            //ALWAYS CHECK AND CACHE MAIN WINDOW
            if (CacheMainWindow.IsNull())
            {
                CacheMainWindow = new T();
                CacheMainWindow.HideOnly = true;
                (CacheMainWindow as Window).Closing += UIManager_Closing;

            }

            IHostWindow hostWindow = CacheMainWindow;
            IView view = GetViewWithCache(viewType, cacheView);

            // Reset VIEW PARAMETERS
            view.Parameters.ResetParameters(viewParameters);

            hostWindow.SetHeader(view.HostHeader);
            hostWindow.SetChild(view as WPF.UserControl);
            view.SetHostWindow(hostWindow);

            try
            {
                  
                if (isBackTrack == false)// ONLY FOR FORWARD TRACK NOT BACK
                {
                    SaveTrack(viewType, cacheView, ShowTypeEn.Show
                        , viewParameters);
                }  
                           
                (hostWindow as Window).Show();
            }
            catch (System.Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void UIManager_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            if (NavTracker.Count >  1)
            {  
                ShowPreviosTrack();
            }
            else if (NavTracker.Count == 1)
            {   
                //(sender as Window).Closing -= UIManager_Closing;
                NavTracker.Pop();                 
            }    
        }



        /// <summary>
        /// Show TView inside Main Host Window.
        /// Main Host Window Always will be cached.
        /// TView  can be cached.
        /// </summary>
        /// <typeparam name="TView">View - UserControl class</typeparam>
        /// <param name="cacheView">say: if this TView instance  will be Cached</param>
        public void Show<TView>(bool cacheView = false
                               , params KeyValuePair<string, object>[] paramValues
                               )
        where TView : WPF.UserControl, IView, new()
        {
            Show(typeof(TView), cacheView, isBackTrack :false, paramValues);
        }





        void ShowDialog(Type viewType
                    , bool cacheDialog = false
                    , bool isBackTrack = false
                    , params KeyValuePair<string, object>[] viewParameters)
        {
            var viewDlgID = GetViewID(viewType);

            IHostWindow hostDialog = null;
            if (cacheDialog && CacheDialogs.ContainsKey(viewDlgID))
            {
                hostDialog = CacheDialogs[viewDlgID];
            }
            else if (cacheDialog == false)
            {
                hostDialog = new T();
                (hostDialog as Window).Closing += UIManager_ClosingDlg;

                IView view = Activator.CreateInstance(viewType) as IView;

                // Reset VIEW PARAMETERS
                view.Parameters.ResetParameters(viewParameters);

                //ATTACHING VIEW
                hostDialog.SetHeader(view.HostHeader);
                hostDialog.SetChild(view as WPF.UserControl);
                view.SetHostWindow(hostDialog);
               
            }
            // GET/ CACHE [Dialog + VIEW]            
            else if (cacheDialog && CacheDialogs.ContainsKey(viewDlgID) == false)
            {
                hostDialog = new T();
                hostDialog.HideOnly = true;

                IView view = Activator.CreateInstance(viewType) as IView;

                // Reset VIEW PARAMETERS
                view.Parameters.ResetParameters(viewParameters);

                //ATTACHING VIEW
                hostDialog.SetHeader(view.HostHeader);
                hostDialog.SetChild(view as WPF.UserControl);
                view.SetHostWindow(hostDialog);
                 
                CacheNewDialog(viewDlgID, hostDialog);
                 
            }

            try
            {
                if (isBackTrack == false)// ONLY FOR FORWARD TRACK NOT BACK
                {
                    SaveTrack(viewType, cacheDialog, ShowTypeEn.ShowDialog
                      , viewParameters);
                }
                               
                (hostDialog as Window).ShowDialog();
            }
            catch (System.Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void UIManager_ClosingDlg(object sender, System.ComponentModel.CancelEventArgs e)
        {            
            if ( (sender as IHostWindow).HideOnly == true )
            {
              e.Cancel = true;
            }
            else // can close 
            {
                (sender as Window).Closing -= UIManager_ClosingDlg;
            }

            if (NavTracker.Count > 1)
            {
                ShowPreviosTrack();
            }
            else if (NavTracker.Count == 1)
            {
                //(sender as Window).Closing -= UIManager_Closing;
                NavTracker.Pop();
            }
        }

        /// <summary>
        /// Show TView inside  Host-Dialog Window .
        /// [Host-Dialog Window + View]  can be cached. 
        /// </summary>
        /// <typeparam name="TView">View - UserControl class </typeparam>
        /// <param name="cacheDialog">tell us: if this [Host-Dialog Window + View]  will be Cached</param>
        /// <param name="paramValues"> parameters on Show </param>
        public void ShowDialog<TView>(bool cacheDialog = false
                    , params KeyValuePair<string, object>[] viewParameters)
            where TView : WPF.UserControl, IView, new()
        {
            ShowDialog(typeof(TView), cacheDialog,isBackTrack:false, viewParameters);
        }

        #endregion  ---------- SHOW  SHOWDIALOG------------


        #region ----------- GET WITH CACHE VIEW OR DIALOG ----------
        public TView GetViewWithCache<TView>(bool cacheView = false)
        where TView : WPF.UserControl, IView, new()
        {
           return GetViewWithCache(typeof(TView), cacheView) as TView;
        }
        public IView GetViewWithCache(Type viewType, bool cacheView = false)
        {
            IView view = null;
            var viewID = GetViewID(viewType);
            // CACHING VIEW
            if (cacheView && CacheViews.ContainsKey(viewID))
            {
                view = CacheViews[viewID] as IView;
            }
            else if (cacheView && CacheViews.ContainsKey(viewID) == false)
            {
                view = Activator.CreateInstance(viewType) as IView;
                CacheNewView(viewID, view as UserControl);
            }
            else if (cacheView == false)
            {
                view = Activator.CreateInstance(viewType) as IView;
            }
            return view;

        }

        void CacheNewView(int viewID, WPF.UserControl viewItem)
        {
            if (MAX_Views > CacheViews.Count)
            {
                CacheViews.Add(viewID, viewItem);
            }
            else if (MAX_Views == CacheViews.Count)
            {
                CacheViews.Remove(0);
                CacheViews.Add(viewID, viewItem);
            }
        }

        void CacheNewDialog(int viewID, IHostWindow dialogItem)
        {
            if (MAX_Dialogs > CacheDialogs.Count)
            {
                CacheDialogs.Add(viewID, dialogItem);
            }
            else if (MAX_Dialogs == CacheDialogs.Count)
            {
                CacheDialogs.Remove(0);
                CacheDialogs.Add(viewID, dialogItem);
            }
        }

        #endregion  ----------- GET WITH CACHE VIEW OR DIALOG ----------
         

    }


}

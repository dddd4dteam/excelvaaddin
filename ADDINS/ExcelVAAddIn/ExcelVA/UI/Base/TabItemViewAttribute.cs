﻿using System;
using System.ComponentModel.Composition;

namespace ExcelVA.UI
{
    
    [AttributeUsage(AttributeTargets.Class  , AllowMultiple =  true)]
    public sealed class TabItemViewAttribute : Attribute
    {

        public TabItemViewAttribute(string tabItemKey, Type viewType)             
        {
            TabItemKey = tabItemKey;
            ViewType  = viewType;
        }


        /// <summary>
        /// TabItemKey of TabControl
        /// </summary>
        public string TabItemKey
        { get; private set; }

        /// <summary>
        /// TabItemKey of TabControl
        /// </summary>
        public Type ViewType
        { get; private set; }

    }
}

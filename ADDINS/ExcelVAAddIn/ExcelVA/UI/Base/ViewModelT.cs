﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ExcelVA;
using ExcelVA.Service;

namespace ExcelVA.UI
{
    public class ViewModel<TView>
        where TView: UserControl,IView, new()
    {
       protected ViewModel(TView associatedView)
        {
            View = associatedView;
            Initialize();
            AttachEventHandlers();
            InitAfterEventsAttached();

        }

        protected virtual void InitAfterEventsAttached()
        {
            
        }


        //public static ViewModel<TView> Create(TView associatedView)
        //{
        //    return new  ViewModel<TView>(associatedView);
        //}


        public TView View
        {  get;  private set;  }

        Dictionary<string, bool> TabInitialized
        { get; set; } = new Dictionary<string, bool>();


        private void Initialize()
        {
            if ( typeof(TView).GetInterface(nameof(ITabCompositeView)).IsNotNull()  )
            {
                var tabItemAttribs = typeof(TView).
                    GetCustomAttributes(typeof(TabItemViewAttribute),false);

                foreach (TabItemViewAttribute tabItemView in tabItemAttribs)
                {
                    var tabItem = View.FindName(tabItemView.TabItemKey) as TabItem;
                    tabItem.Loaded  += (s, e) =>
                    {
                        if (TabInitialized.ContainsKey(tabItemView.TabItemKey) == false)
                        {
                            VAService.Current.UIM.SetTabItem(
                            tabItem, tabItemView.ViewType);
                            TabInitialized.Add(tabItemView.TabItemKey, true);
                        }
                    };
                   
                }
            }          
        }

        protected virtual void AttachEventHandlers()
        {             
        }




    }
}

﻿using System.Collections.Generic;
using System.Windows;

namespace ExcelVA.UI
{
    /// <summary>
    ///  View Parameters 
    /// </summary>
    public class ViewParameters
    {
        #region ----------- VIEW PARAMETERS--------
        Dictionary<string, object> Parameters
        { get; } = new Dictionary<string, object>();
        
        /// <summary>
        /// Get Parameter Value by Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object GetParamVal(string key)
        {
            if (Parameters.ContainsKey(key))
            {
                return Parameters[key];
            }
            else return null;
        }

        /// <summary>
        /// Reset all Parameters - clear all existed and then add new parameters
        /// </summary>
        /// <param name="paramValues"></param>
        public void  ResetParameters(params KeyValuePair<string, object>[] paramValues)
        {
            Parameters.Clear();
            if (paramValues.Length == 0) return;

            for (int i = 0; i < paramValues.Length; i++)
            {
                AddOrChangeParameter(paramValues[i].Key, paramValues[i].Value);
            }
        }

        /// <summary>
        /// Add if not exist or change if exist parameter with [key]
        /// </summary>
        /// <param name="key"></param>
        /// <param name="paramValue"></param>
        public void AddOrChangeParameter(string key, object paramValue)
        {
            if (Parameters.ContainsKey(key))
            {
                Parameters[key] = paramValue;
            }
            else Parameters.Add(key, paramValue);
        }

        /// <summary>
        /// Clear parameters
        /// </summary>
        public void ClearParameters()
        {
            Parameters.Clear();
        }

        #endregion ----------- VIEW PARAMETERS--------


    }

    public interface IView
    {
        IHostWindow HostWindow
        { get; }
        void SetHostWindow(IHostWindow hostWindow);

        string HostHeader { get; }

        ViewParameters Parameters { get; }

    }



}

﻿namespace ExcelVA.UI
{

    public interface IDialogHostWindow
    {
        void CloseOK();

        void CloseCancel();
    }

}

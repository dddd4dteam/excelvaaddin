﻿
using ExcelVA.XL.Model;
using System;
using System.Globalization;
using System.Windows.Data;
 

namespace ExcelVA.UI.Converters
{

    /*
     <Window x:Class="VirtualControlDemo.Window1"
    ...
    xmlns:l="clr-namespace:VirtualControlDemo"
    ...>
    <Window.Resources>
        <l:BoolToVisibilityConverter x:Key="converter" />
    </Window.Resources>
    <Grid>
        <Button Visibility="{Binding HasFunction, 
            Converter={StaticResource converter}}" />
    </Grid>
</Window>

     */



    public class CellToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            // Do the conversion from bool to visibility
            // Convert from Cell to string

            if (value == null) return Cell.FirstCell;
             
                var cellValue = value as Cell;
           return cellValue.Adres.Value;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            // Do the conversion from visibility to bool
            // Convert from string to Cell 
            var adressCellValue = value as string;
            return  new Cell(adressCellValue);
        }
    }

   
}

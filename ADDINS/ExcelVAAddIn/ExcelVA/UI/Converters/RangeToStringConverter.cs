﻿
using ExcelVA.XL.Model;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ExcelVA.UI.Converters
{
    public class RangeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
           object parameter, CultureInfo culture)
        {
            // Do the conversion from bool to visibility
            // Convert from Range to string
            if (value == null) return Range.FirstRange;

            var rangeValue = value as Range;
            return rangeValue.Address;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            // Do the conversion from visibility to bool
            // Convert from string to Range 
            var adressRangeValue = value as string;
            return new Range(adressRangeValue);
        }

    }
}

﻿ 
using ExcelVA.Binds;
using ExcelVA.Processing;
using ExcelVA.UI;
using ExcelVA.Views;
using ExcelVA.XL;

using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
using WPF = System.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelVA.Service;
using System.Windows;
using ExcelVA.XL.Model;

namespace ExcelVA.Views
{
    /// <summary>
    /// View Model for  View_EditSourceFile UserControl
    /// </summary>
    public class View_EditSourceFileVM : ViewModel<View_EditSourceFile>
    {

        #region ------------- CTOR ----------------
        public View_EditSourceFileVM(View_EditSourceFile associatedView)
            : base(associatedView)
        {

        }
        #endregion ------------- CTOR ----------------
         
        protected override void AttachEventHandlers()
        {
            View.Loaded += View_Loaded;

           
            View.btn_Select_SourceIdentityDataRange.Click += Btn_Select_SourceIdentityDataRange_Click;
           
            View.btn_Check_DataColumns.Click += Btn_Check_DataColumns_Click;

            View.btn_Select_FilterSrcCol.Click += Btn_Select_FilterSrcCol_Click;
            View.btn_Select_FilterTrgCol.Click += Btn_Select_FilterTrgCol_Click;
            View.btn_Select_FilterValue.Click += Btn_Select_FilterValue_Click;

            View.btn_OK.Click += Btn_OK_Click;
            
        }


        /// <summary>
        /// Data Copying Data Context Info
        /// </summary>
        MultipleDataCopying ProcDataContext
        { get; set; }

        DataCopying  CopyTask
        {   get
            {
                return ProcDataContext.SelectedTask;
            } 
        }
        protected override void InitAfterEventsAttached()
        {
            // Init  -  ProcDataContext
            ProcDataContext = MultipleDataCopying.GetOrCreateFromBSGlobal();// GetOrCreateFromBS();
        }

        private void View_Loaded(object sender, WPF.RoutedEventArgs e)
        {
            // Load this excel source file
            ExcelApp.LoadMainWB(CopyTask.SourceFilePath);
            
            //Activate last sheet if it is not null
            if (CopyTask.SourceSheetName != null )
            {
                var actWorkSheet =  ExcelApp.App.ActiveWorkbook.Worksheets[CopyTask.SourceSheetName] as Excel.Worksheet;
                if (actWorkSheet  != null)
                {  actWorkSheet.Activate();
                }
            }
        }

        #region ----------- SET ADRESSES ------------
        protected void SetAdress_ToSourceIdentityDataRange(string selectedAddress)
        {
            View.tbx_SourceIdentityDataRange.Text = selectedAddress;
        }
        protected void SetAdress_ToFltrSrc(string selectedAddress)
        {
            View.tbx_FilterSrcCol.Text = selectedAddress;   
        }
        protected void SetAdress_ToFltrTrg(string selectedAddress)
        {
            View.tbx_FilterTrgCol.Text = selectedAddress;
        }

        protected void SetAdress_ToFltrVal(string selectedAddress)
        {
            View.tbx_FilterValue.Text = selectedAddress;
        }
        


        #endregion ----------- SET ADRESSES ------------
        private void Btn_Select_SourceIdentityDataRange_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Action<string> setAct = SetAdress_ToSourceIdentityDataRange;
            VAService.Current.UIM.Show<View_Selection>(cacheView: true
             , new KeyValuePair<string, object>(SelectionParams.SetAdressAct, setAct)
             , new KeyValuePair<string, object>(SelectionParams.IsCellOrRange, "Range")
             );
        }
         

        #region ------------- FILTER HANDLERS --------------
        private void Btn_Select_FilterSrcCol_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Action<string> setAct = SetAdress_ToFltrSrc;
            VAService.Current.UIM.Show<View_Selection>(cacheView: true
             , new KeyValuePair<string, object>(SelectionParams.SetAdressAct, setAct)
             , new KeyValuePair<string, object>(SelectionParams.IsCellOrRange, "Cell")
             );
        }
        private void Btn_Select_FilterTrgCol_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Action<string> setAct = SetAdress_ToFltrTrg;
            VAService.Current.UIM.Show<View_Selection>(cacheView: true
             , new KeyValuePair<string, object>(SelectionParams.SetAdressAct, setAct)
             , new KeyValuePair<string, object>(SelectionParams.IsCellOrRange, "Cell")
             );
        }
        private void Btn_Select_FilterValue_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Action<string> setAct = SetAdress_ToFltrVal;
            VAService.Current.UIM.Show<View_Selection>(cacheView: true
             , new KeyValuePair<string, object>(SelectionParams.SetAdressAct, setAct)
             , new KeyValuePair<string, object>(SelectionParams.IsCellOrRange, "Cell")
             );
        }

        #endregion ------------- FILTER  HANDLERS --------------

        private void Btn_OK_Click(object sender, WPF.RoutedEventArgs e)
        {
            if (CheckDataColumnsMap() == false)
            {  return;  }

            (View.HostWindow as WPF.Window).Hide();
        }

        private void Btn_Check_DataColumns_Click(object sender, WPF.RoutedEventArgs e)
        {
            CheckDataColumnsMap();             
        }

        private bool CheckDataColumnsMap()
        {
            var formatMsgPart = "\r\n" + "Используйте Формат [ (WsInd)!S-(WsInd)!T] "
                              + "\r\n" + "где S- Столбец в Исходном файле, T- Столбец в целевом файле "
                              + "\r\n" + " (WsInd) - индекс , начиная нумерацию с 1";
            if (string.IsNullOrEmpty(View.tbx_DataColumns.Text) )
            {
                MessageBox.Show(
                    "Сначала ввелите хоть одну связку стобцов данных." + formatMsgPart );
                return false;                    
            }

            var mapItems = View.tbx_DataColumns.Text.SplitNoEntries(Symbols.Zpt);
            if (mapItems != null  &&  mapItems.Count == 0)  
            {
                MessageBox.Show(
                   "Сначала введите хоть одну связку стобцов данных" + formatMsgPart);
                return false;
            }

            for (int i = 0; i < mapItems.Count; i++)
            {
                //parse mappint Items
                var clmns = mapItems[i].SplitNoEntries(Symbols.Mns);
                if (clmns == null || clmns.Count != 2)
                {   MessageBox.Show(
                    $" Некорректная связка-[{i.S()}]." + formatMsgPart);
                    
                    return false;
                }
                // Cell adresses  for columns
                CopyTask.DataColumnsMap.Add(
                    new KeyValuePair<Cell, Cell>(
                            new Cell( clmns[0] + "1")
                          , new Cell( clmns[1] + "1") )  
                    ); 
            }

            return true;
        }
    }


     



}

﻿ 
using ExcelVA.Binds;
using ExcelVA.Processing;
using ExcelVA.Service;
using ExcelVA.UI;
using ExcelVA.Views;
using ExcelVA.XL;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExcelVA.Views
{
    /// <summary>
    /// View Model for View_Main_Checks UserControl
    /// </summary>
    public class View_Main_DataCopyVM : ViewModel<View_Main_DataCopy>
    {
        public View_Main_DataCopyVM(View_Main_DataCopy associatedView)
            : base(associatedView)
        {

        }

        protected override void AttachEventHandlers()
        {
            View.btn_Select_DataCopyConfigFile.Click += Btn_Select_DataCopyConfigFile_Click;
            View.btn_Save_DataCopyConfigFile.Click += Btn_Save_DataCopyConfigFile_Click;

            View.btn_SelectOtchetFile.Click += Btn_SelectOtchetFile_Click;

            View.btn_SelectTargetIdentityDataRange.Click += Btn_SelectTargetIdentityDataRange_Click;
            
            View.btn_ChangeSourceFile.Click += Btn_ChangeSourceFile_Click;
            View.btn_DeleteSourceFile.Click += Btn_DeleteSourceFile_Click;

        }
        protected override void InitAfterEventsAttached()
        {
            // Init Data Contect - MultipleDataCopy in BS
            ProcDataContext = MultipleDataCopying.GetOrCreateFromBSGlobal();
            
            // Change Current Processing   object  
            BSGlobal.SetValue(BSGlobal.Key.CurrentProcessing, ProcDataContext);

        }



        #region ------------ SELECT RANGE --------------
        protected void SetAdress_ToTargetIdentityDataRange(string selectedAddress)
        {
            View.tbx_TargetIdentityDataRange.Text = selectedAddress;
        }
        private void Btn_SelectTargetIdentityDataRange_Click(object sender, RoutedEventArgs e)
        {
            Action<string> setAct = SetAdress_ToTargetIdentityDataRange;
            
            VAService.Current.UIM.Show<View_Selection>(cacheView: true
             , new KeyValuePair<string, object>(SelectionParams.SetAdressAct, setAct)
             , new KeyValuePair<string, object>(SelectionParams.IsCellOrRange, "Range")
             );
        }

        #endregion ------------ SELECT RANGE --------------  









        /// <summary>
        /// Data Copying Data Context Info
        /// </summary>
        MultipleDataCopying ProcDataContext 
        { get; set; } 

        private void Btn_Select_DataCopyConfigFile_Click(object sender, System.Windows.RoutedEventArgs e)
        {    
            if (Directory.Exists(MultipleDataCopying.InitialDir) == false )
            {  Directory.CreateDirectory(MultipleDataCopying.InitialDir);
            }

            var openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = MultipleDataCopying.InitialDir;
            openFileDialog.Filter = MultipleDataCopying.CofigFileFilter;
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog. ShowDialog() == true)
            {
                if (string.IsNullOrEmpty(openFileDialog.FileName)== false)
                {
                    ProcDataContext.DataCopyConfigFilePath =  openFileDialog.FileName;
                    MultipleDataCopying.LoadFromJson(ProcDataContext);
                    ProcDataContext = MultipleDataCopying.GetOrCreateFromBSGlobal();

                    //if Binding broke try set to Data Context manually
                    View.DataContext = ProcDataContext;
                }   
            }
        }
  
        private void Btn_Save_DataCopyConfigFile_Click(object sender, System.Windows.RoutedEventArgs e)
        {           

            if (Directory.Exists(MultipleDataCopying.InitialDir) == false)
            {   Directory.CreateDirectory(MultipleDataCopying.InitialDir);
            }

            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory =
                ProcDataContext.DataCopyConfigFileDir;
            saveFileDialog.Filter = MultipleDataCopying.CofigFileFilter;
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.FileName = MultipleDataCopying.CofigFileNameDef;
           
            if (saveFileDialog.ShowDialog() == true)
            {
                if (string.IsNullOrEmpty(saveFileDialog.FileName) == false)
                {
                    ProcDataContext.DataCopyConfigFile = new FileInfo(saveFileDialog.FileName);
                    
                    // Save Processing object as json config file 
                    MultipleDataCopying.SaveAsJson(ProcDataContext);

                    ProcDataContext.IsDataCopyConfigFileSaved = true;
                }               
            } 
        }
         


        private void Btn_SelectOtchetFile_Click(object sender, System.Windows.RoutedEventArgs e)
        { 
            // select otchet file
            // load all other excel source files into list

            if (Directory.Exists(MultipleDataCopying.InitialDir) == false)
            {
                MessageBox.Show("Директория не существует");
                return;     
            }

            var openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = MultipleDataCopying.InitialDir;
            openFileDialog.Filter = ExcelApp.ExcelFileFilter;
                 

            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                ProcDataContext.TargetOtchetFilePath = openFileDialog.FileName;
            }

            // Load source Files as DataCopyTask
           
            LoadSourceFilesTask();

            // update/create auto config file path
            var targetCfgFile =
                    ProcDataContext.TargetOtchetFile.Name.Replace(
                        ProcDataContext.TargetOtchetFile.Extension
                        , MultipleDataCopying.CofigFileExt);

            ProcDataContext.DataCopyConfigFile = new FileInfo(targetCfgFile);
             
        }

        private void LoadSourceFilesTask()
        {
          var allFiles =  ProcDataContext.TargetOtchetFile.Directory.GetFiles();

            for (int i = 0; i < allFiles.Length; i++)
            {
               var curFile = allFiles[i];
                //if it is TARGET FILE skip this file
                if (curFile.Name == ProcDataContext.TargetOtchetFile.Name)
                {   continue; }
                
                //if Excel temp file skip this file
                if (curFile.Name.StartsWith("~$") )
                { continue; }
                
                //if not only Excel  file by Extension skip this file 
                if ( ExcelApp.IsExcelFile(curFile.Extension) == false )
                {  continue; }

                // copy task for this file already was created
                if (ProcDataContext.CopyTasks
                    .Where(ct=>ct.SourceFile.Name == curFile.Name).FirstOrDefault() != null )
                {  continue; }

                //create new Data Copy Task                 
                ProcDataContext.CopyTasks.Add(
                    new DataCopying()
                    { SourceFilePath = curFile.FullName } );

            } 

        }

        


        private void Btn_DeleteSourceFile_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (ProcDataContext.SelectedTaskIndex != -1  )
            {
                ProcDataContext.CopyTasks.RemoveAt(ProcDataContext.SelectedTaskIndex);
                MessageBox.Show("Источник удален");
            }
            else 
            {
                MessageBox.Show(" Не выбран элемент для удаления");
            }    
        }

        private void Btn_ChangeSourceFile_Click(object sender, RoutedEventArgs e)
        {   
            VAService.Current.UIM.Show<View_EditSourceFile>(cacheView: true);
        }


    }
}

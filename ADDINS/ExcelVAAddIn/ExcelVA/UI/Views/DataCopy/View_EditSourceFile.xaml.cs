﻿using ExcelVA.Resources;
using ExcelVA.UI;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ExcelVA.Views
{
    /// <summary>
    /// Логика взаимодействия для  View_EditSourceFile.xaml
    /// </summary>
    public partial class View_EditSourceFile : UserControl, IView
    {
        public View_EditSourceFile()
        {
            InitializeComponent();
            ViewModel = new View_EditSourceFileVM(this); 
        }

        public ViewModel<View_EditSourceFile> ViewModel
        { get; private set; }

        public ViewParameters Parameters
        { get; } = new ViewParameters();
        public string HostHeader
        { get; private set; } = RC.VH_MainWnd;


        public IHostWindow HostWindow
        { get; private set; }

        public void SetHostWindow(IHostWindow hostWindow)
        {
            HostWindow = hostWindow;
        }


         

        private void Btn_MainWindow_Click(object sender, RoutedEventArgs e)
        {
               
        }

        private void Btn_RangeWindow_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_SavingWindow_Click(object sender, RoutedEventArgs e)
        {

        }



        // MERGED CELLS INFO
        // Check Cell IS MERGED
        // Get MERGED-Cell Value
        // Get MERGED-Cell Range


        private void Btn_IsMerged_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_CellValue_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_CellRamge_Click(object sender, RoutedEventArgs e)
        {

        }


    }
}

﻿using ExcelVA.Resources;
using ExcelVA.UI;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ExcelVA.Views
{
    /// <summary>
    /// Логика взаимодействия для  View_CheckHeaderCells.xaml
    /// </summary>
    public partial class View_CheckHeaderCells : UserControl, IView
    {

        public View_CheckHeaderCells()
        {
            InitializeComponent();
        }
        public ViewParameters Parameters
        { get; } = new ViewParameters();
        public string HostHeader
        { get; protected set; } = RC.VH_CheckHeaderCells;

        
        public IHostWindow HostWindow
        { get; private set; }

        public void SetHostWindow(IHostWindow hostWindow)
        {
            HostWindow = hostWindow;
        }

         

    }
}

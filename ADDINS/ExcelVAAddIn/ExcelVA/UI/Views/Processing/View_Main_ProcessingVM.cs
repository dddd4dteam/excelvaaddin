﻿using ExcelVA.Binds;
using ExcelVA.Processing;
using ExcelVA.Service;
using ExcelVA.UI;
using ExcelVA.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ExcelVA.Views
{
    /// <summary>
    /// View Model for View_Main_Processing UserControl
    /// </summary>
    public class View_Main_ProcessingVM : ViewModel<View_Main_Processing>
    {
        public View_Main_ProcessingVM(View_Main_Processing associatedView)
            : base(associatedView)
        {

        }

        protected override void AttachEventHandlers()
        {
           View.btn_StartCheckingProcess.Click += Btn_StartCheckingProcess_Click;
           View.btn_StopCheckingProcess.Click += Btn_StopCheckingProcess_Click;
        }
        protected override void InitAfterEventsAttached()
        {
            
            // BUSY INFORMING
            View.prgbr_CommonProgress.SayInform = SayInformUI;
            View.prgbr_CommonProgress.SayWarning = SayWarningUI;
            View.prgbr_CommonProgress.SayError = SayErrorUI;

            VAService.Current.AttacheBusyControl(
              View.prgbr_CommonProgress);

        }


        void SayInformUI(string inform)
        {
            View.lbx_Errors.Items.Add(
                new ListBoxItem()
                {
                    Content = "INFORM:" + inform,
                    ToolTip = "INFORM:" + inform
                }
            );
        }
        void SayWarningUI(string warning)
        {
            View.lbx_Errors.Items.Add(
                new ListBoxItem()
                {
                    Content = "WARNING:" + warning,
                    ToolTip = "WARNING:" + warning
                }
            );
        }
        void SayErrorUI(string error)
        {
            View.lbx_Errors.Items.Add( 
                    new ListBoxItem()
                    {   Content = "ERROR:" + error,
                        ToolTip = "ERROR:" + error
                    } 
                );
        }


        private void Btn_StartCheckingProcess_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            VAService.Current.StartProc(stage: 1);// 
        }
        private void Btn_StopCheckingProcess_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            VAService.Current.StopProc();
        }       



        /// <summary>
        /// Data Copying/Cells analysing  processing
        /// </summary>
        public IProcess CurProcessing
        { get { return
                    BSGlobal.GetValue(BSGlobal.Key.CurrentProcessing) as IProcess; }
        }


        /// <summary>
        /// Progres Reporter
        /// </summary>
        public ProgressReporter Progres
        { 
            get { return VAService.Current.Reporter;  } 
        } 


    }
}

﻿using ExcelVA.Resources;
using ExcelVA.UI;
using ExcelVA.XL;
using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using WPF = System.Windows;
using System.Windows.Controls;

namespace ExcelVA.Views
{
    /// <summary>
    /// Логика взаимодействия для  View_ChecksList.xaml
    /// </summary>
    public partial class View_Selection  : UserControl, IView
    {
         
        public View_Selection()
        {
            InitializeComponent();
            ViewModel = new View_SelectionVM(this);
        }

        public ViewModel<View_Selection> ViewModel
        { get; private set; }
        public ViewParameters Parameters
        { get; } = new ViewParameters();
        public string HostHeader
        { get; protected set; } = RC.VH_CheckList;

        public IHostWindow HostWindow
        { get; private set; }
        
        public void SetHostWindow(IHostWindow hostWindow)
        {
            HostWindow = hostWindow;
        }
        
        
    }
}

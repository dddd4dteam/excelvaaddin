﻿using ExcelVA.UI;
using ExcelVA.XL;
using Microsoft.Office.Interop.Excel;
using System;
using System.Windows;
using WPF = System.Windows;

namespace ExcelVA.Views
{

    /// <summary>
    /// View_Selection parameters to set selected adress o sheet to Target Control.
    /// </summary>
    public class SelectionParams
    {

       /// <summary>
       /// on OK- call this Set Action(string) value - where we set Selected address on selectedActive sheet to target  textbox.Text  property of end View  
       /// </summary>
       public  const string SetAdressAct = nameof(SetAdressAct);
    

       /// <summary>
       /// Param should Contains value [Cell] for cell or [Range] for range adress as output
       /// </summary>
       public const string IsCellOrRange = nameof(IsCellOrRange);

    }


    /// <summary>
    /// View Model for View_Main_Checks UserControl
    /// </summary>
    public class View_SelectionVM : ViewModel<View_Selection>
    {
        public View_SelectionVM(View_Selection associatedView ) 
            :base(associatedView)
        {

        }

        double ViewWidth = 220;
        double ViewHeight = 100;
        double HostWidth = 0;
        double HostHeight = 0;
       
         Action<string> AdressAct = null;
         string CellOrRange = null;
        protected override void AttachEventHandlers()
        {
            View.btn_OK.Click += Btn_OK_Click;
            View.Loaded += View_Loaded;
            ExcelApp.App.SheetSelectionChange += App_SheetSelectionChange;

        }

        protected override void InitAfterEventsAttached()
        {            
            // set Parameeter values of Parent View- to set results 
        }

        private void View_Loaded(object sender, WPF.RoutedEventArgs e)
        {
            AdressAct = (Action<string>)View.Parameters.GetParamVal(SelectionParams.SetAdressAct);
            CellOrRange = View.Parameters.GetParamVal(SelectionParams.IsCellOrRange) as string;


            HostWidth = (View.HostWindow as WPF.Window).Width;
            HostHeight = (View.HostWindow as WPF.Window).Height;

            (View.HostWindow as WPF.Window).Width = ViewWidth + 10;
            (View.HostWindow as WPF.Window).Height = ViewHeight + 10;
            (View.HostWindow as WPF.Window).WindowStyle = WPF.WindowStyle.None ; 
            (View.HostWindow as WPF.Window).Topmost = true;
            (View.HostWindow as WPF.Window).MouseDown += View_SelectionVM_MouseDown;
             
        }

        private void View_SelectionVM_MouseDown(object sender, WPF.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == WPF.Input.MouseButton.Left)
                (View.HostWindow as WPF.Window).DragMove();
        }


       

        private void App_SheetSelectionChange(object Sh, Microsoft.Office.Interop.Excel.Range Target)
        {
            //Set Selection To FilterSrcCol 
            View.tbx_CellOrRange.Text = Target.AddressLocal.Replace("$","");
            View.tbx_CellOrRange.Tag = Target.Worksheet.Name;
                
        }

        private void Btn_OK_Click(object sender, WPF.RoutedEventArgs e)
        {
            if (CheckAndSetAddress()== false)
            {   return;   }

            //Check input data Cell/ Range
            (View.HostWindow as WPF.Window).MouseDown -= View_SelectionVM_MouseDown;
            (View.HostWindow as WPF.Window).Close();
            (View.HostWindow as WPF.Window).Width = HostWidth;
            (View.HostWindow as WPF.Window).Height = HostHeight;
            (View.HostWindow as WPF.Window).WindowStyle = WPF.WindowStyle.SingleBorderWindow;
            (View.HostWindow as WPF.Window).Topmost  = false;
            
            CenterWindowOnScreen();
        }

        private bool CheckAndSetAddress()
        {
            bool isCell = (CellOrRange == "Cell");
            bool isRange = (CellOrRange == "Range");
            if (isCell && View.tbx_CellOrRange.Text.Contains(":"))
            {
                MessageBox.Show("Надо выбрать адрес ячейки а не дипазонв");
                return false;
            }
            if (isRange && View.tbx_CellOrRange.Text.Contains(":") == false)
            {
                MessageBox.Show("Надо выбрать адрес диапазона а не ячейки");
                return false;
            } 

            // set to target 
            if (AdressAct == null)
            {
                MessageBox.Show("Не задан параметр сеттера адреса");
            }
            else 
            {
                var wrksht = View.tbx_CellOrRange.Tag as string;
                var cellRangeAdr = View.tbx_CellOrRange.Text;
                if (isCell)
                { // [Worksheet!CellAdr] 
                    var adress = $"{wrksht}{Symbols.ShDm}{cellRangeAdr}";
                    AdressAct(adress);
                }
                if (isRange)
                {   var rangeAdress = AttachSheetNameToRangeAdres(cellRangeAdr,wrksht);  
                    AdressAct(rangeAdress);
                }
            }

            return true;
        }

        private string AttachSheetNameToRangeAdres(string rangeAdr, string workSheetName )
        {
            var cells = rangeAdr.SplitNoEntries(":");
            cells[0] =  $"{workSheetName}!{cells[0]}";
            cells[1] = $"{workSheetName}!{cells[1]}";
            return $"{cells[0]}:{cells[1]}"; 
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = (View.HostWindow as WPF.Window).Width;
            double windowHeight = (View.HostWindow as WPF.Window).Height;
            (View.HostWindow as WPF.Window).Left = (screenWidth / 2) - (windowWidth / 2);
            (View.HostWindow as WPF.Window).Top = (screenHeight / 2) - (windowHeight / 2);
        }

    }
}

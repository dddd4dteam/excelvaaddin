﻿using ExcelVA.Resources;
using ExcelVA.Service;
using ExcelVA.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ExcelVA.Views
{


    /*
     <TabItem x:Name="tabItm_Checks" Visibility="Hidden">
                <TabItem.Header>
                    <StackPanel Orientation="Horizontal">
                        <Ellipse Height="10" Width="10" Fill="Black" />
                        <TextBlock Margin="3"
                            Text="Проверки" 
                                   ToolTip="Выбор файлов Excel и настройка проверок"
                        />
                    </StackPanel>
                </TabItem.Header>            
            </TabItem>
     */
    /// <summary>
    /// Логика взаимодействия для View_Main.xaml
    /// </summary>
    //[TabItemView("tabItm_Checks", typeof(View_Main_Checks) )]
    [TabItemView("tabItm_DataCopy", typeof(View_Main_DataCopy))]
    [TabItemView("tabItm_Processing", typeof(View_Main_Processing ))]   
    public partial class View_Main : UserControl, IView, ITabCompositeView
    {
        public View_Main()
        {
            InitializeComponent();
            ViewModel = new View_MainVM(this); 
        }
         
        public ViewModel<View_Main> ViewModel
        { get; private set; }
        public ViewParameters Parameters
        { get; } = new ViewParameters();

        public string HostHeader
        { get; private set; } = RC.VH_MainWnd;


        public IHostWindow HostWindow
        { get; private set; }

        public void SetHostWindow(IHostWindow hostWindow)
        {
            HostWindow = hostWindow;
        }

       

        private void Btn_MainWindow_Click(object sender, RoutedEventArgs e)
        {
               
        }

        private void Btn_RangeWindow_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_SavingWindow_Click(object sender, RoutedEventArgs e)
        {

        }



        // MERGED CELLS INFO
        // Check Cell IS MERGED
        // Get MERGED-Cell Value
        // Get MERGED-Cell Range


        private void Btn_IsMerged_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_CellValue_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_CellRamge_Click(object sender, RoutedEventArgs e)
        {

        }


       
    }
}

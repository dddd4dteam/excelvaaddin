﻿using ExcelVA.Resources;
using ExcelVA.UI;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ExcelVA.Views
{
    /// <summary>
    /// Логика взаимодействия для  View_Instruction.xaml
    /// </summary>
    public partial class View_Instruction : UserControl, IView
    {
       
        public View_Instruction()
        {
            InitializeComponent();             
        }
        public ViewParameters Parameters
        { get; } = new ViewParameters();
        public string HostHeader
        { get; protected set; } = RC.VH_Instruction;


        public IHostWindow HostWindow
        { get; private set; }

        public void SetHostWindow(IHostWindow hostWindow)
        {
            HostWindow = hostWindow;
        }


         

    }
}

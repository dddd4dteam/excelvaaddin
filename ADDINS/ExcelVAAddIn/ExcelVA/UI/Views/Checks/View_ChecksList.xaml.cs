﻿using ExcelVA.Resources;
using ExcelVA.UI;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ExcelVA.Views
{
    /// <summary>
    /// Логика взаимодействия для  View_ChecksList.xaml
    /// </summary>
    public partial class View_ChecksList : UserControl, IView
    {


        public View_ChecksList()
        {
            InitializeComponent();
        }
        public ViewParameters Parameters
        { get; } = new ViewParameters();
        public string HostHeader
        { get; protected set; } = RC.VH_CheckList;

        public IHostWindow HostWindow
        { get; private set; }
        
        public void SetHostWindow(IHostWindow hostWindow)
        {
            HostWindow = hostWindow;
        }

         

    }
}

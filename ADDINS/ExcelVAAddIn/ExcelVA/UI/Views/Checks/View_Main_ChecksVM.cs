﻿using ExcelVA.UI;
using ExcelVA.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Views
{ 
    /// <summary>
    /// View Model for View_Main_Checks UserControl
    /// </summary>
    public class View_Main_ChecksVM : ViewModel<View_Main_Checks>
    {
       public View_Main_ChecksVM(View_Main_Checks associatedView ) 
            :base(associatedView)
        {

        } 

    }
}

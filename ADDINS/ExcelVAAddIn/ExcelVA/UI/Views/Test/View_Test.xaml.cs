﻿using ExcelVA.Resources;
using ExcelVA.UI;
using ExcelVA.XL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace ExcelVA.Views
{
    /// <summary>
    /// Логика взаимодействия для View_Test.xaml
    /// </summary>
    public partial class View_Test : UserControl, IView
    {
        public View_Test()
        {
            InitializeComponent();            
        }
        public ViewParameters Parameters
        { get; } = new ViewParameters();
        public string HostHeader
        { get; private set; } = RC.VH_TestWnd;



        public IHostWindow HostWindow
        { get; private set; }

        public void SetHostWindow(IHostWindow hostWindow)
        {
            HostWindow = hostWindow;
        }

         




        // MERGED CELLS INFO
        // Check Cell IS MERGED
        // Get MERGED-Cell Value
        // Get MERGED-Cell Range


        static void FileOUT(string messagelines)
        {
            var path = "c:\\Temp\\testout.txt";

            File.AppendAllText(path, messagelines + Environment.NewLine);

        }



        private void Btn_Test1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Load File  
                var path = "d:\\EXCEL\\TEST\\Test333.xlsx";
                IGXL.LoadWorkBook(path);
                IGXL.LoadWorkSheet("Динамика ТЭП отчет");

                // read cells 
                for (int i = 8; i < 108; i++)
                {
                    var adres = "B" + i.S();
                    var cell = IGXL.GetCell(adres);
                    var text = cell.GetText();

                    FileOUT(" Cell addres is [{0}]   Value is [{1}] \n"
                       .Fmt(adres, text));

                }

                IGXL.CleanAll();
            }
            catch (System.Exception exc)
            {
                MessageBox.Show(exc.Message);                
            }
           
        }

        private void Btn_Test2_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_Test3_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

﻿using ExcelVA.Processing;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace ExcelVA.UI.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UCWorkbook.xaml
    /// </summary>
    public partial class UCWorkbook : UserControl, IBusyTotalControl
    {
        public UCWorkbook()
        {
            InitializeComponent();
        }

        public Action<string> SayWarning
        { get; set; }
        public Action<string> SayError
        { get; set; }
        public Action<string> SayInform
        { get; set; }
        public void ChangeProgress(ProgressInfo progres)
        {
            if (progres.Items.ContainsKey(PIKey.Inform))
            {
                //TO DO         
            }
            if (progres.Items.ContainsKey(PIKey.Warning))
            {
               //TO DO         
            }
            if (progres.Items.ContainsKey(PIKey.Error))
            {
                //TO DO
            }


            if (progres.Items.ContainsKey(PIKey.TotalProgress))
            {
                //prgbar
                prgBr_WBookProgress.Value =
               (double)progres.Items[PIKey.TotalProgress];
            
                //Title
                txBlk_Title.Text = progres.Items[PIKey.TotalTitle] as string; 
            }

            if (progres.Items.ContainsKey(PIKey.ItemProgress))
            {
                int index = (int)progres.Items[PIKey.ItemIndex];    
                var busyItem = lBx_Checks.Items[index] as IBusyItemControl;
                busyItem.ChangeProgress(progres);
            }

        }

        public void LoadCheckItems()
        {
            //throw new NotImplementedException();
        }
    }
}

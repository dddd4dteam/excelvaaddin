﻿using ExcelVA.Processing;
using System.Windows.Controls;

namespace ExcelVA.UI.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UC_CheckItem.xaml
    /// </summary>
    public partial class UCCheckItem : UserControl, IBusyItemControl
    {
        public UCCheckItem()
        {
            InitializeComponent();
        }

        public void ChangeProgress(ProgressInfo progres)
        {
            //prgrsBar
            prgBr_CheckProgress.Value = 
                (double)progres.Items[PIKey.ItemProgress];
                                   
            //Title
            txBlk_Title.Text = progres.Items[PIKey.ItemTitle] as string; 
           
        }


    }
}

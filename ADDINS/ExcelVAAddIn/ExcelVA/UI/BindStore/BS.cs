﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Binds
{



    /// <summary>
    /// Binding Object Stores.
    /// Store is ObservableDictionary. 
    /// </summary>
    public static class BS
    {

        /// <summary>
        ///  Binding Keys for known storing objects
        /// </summary>
        public class Key
        {
            public const string WBook = nameof(WBook);

            public const string CheckInfs = nameof(CheckInfs);
        }

        public static Dictionary<string, object> Store
        { get;  } = new Dictionary<string, object>();

        //public static ObservableDictionary<string, object> Store2
        //{ get; } = new ObservableDictionary<string, object>();



    }
}

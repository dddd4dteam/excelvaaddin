﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Binds
{



    /// <summary>
    /// Binding Object Stores.
    /// Store is ObservableDictionary. 
    /// </summary>
    public  class BSGlobal
    {

        /// <summary>
        ///  Binding Keys for known storing objects
        /// </summary>
        public class Key
        {
            
            /// <summary>
            /// Current  Processing 
            /// </summary>
            public static string CurrentProcessing = nameof(CurrentProcessing);


            /// <summary>
            /// WorkBook  checking  processing
            /// </summary>
            public const string WBook = nameof(WBook);

           
            /// <summary>
            /// Data Copy file Processing
            /// </summary>
            public const string DataCopying = nameof(DataCopying);


            public const string CheckInfs = nameof(CheckInfs);
        
        }

        static   ObservableDictionary<string, object> store
          = new ObservableDictionary<string, object>();

        public ObservableDictionary<string, object> Store
        {
            get { return store; }
            //set { store = value; }
        }
        public static object GetValue(string key)
        {           
            return  store[key];
        }

        public static bool ContainsKey(string key)
        {
            return store.ContainsKey(key);
        }

        public static void SetValue(string key, object value)
        {
            store[key] = value;
        }

        public static void Clear()
        {
            store.Clear(); 
        }

    }
}

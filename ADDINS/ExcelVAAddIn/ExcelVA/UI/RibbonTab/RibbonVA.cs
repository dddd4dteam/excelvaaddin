﻿
using ExcelVA.Processing;
using ExcelVA.Service;
using ExcelVA.Views;
using Microsoft.Office.Tools.Ribbon;

namespace ExcelVA.UI
{
    public partial class RbnVA
    {
        private void RbnVAH_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void Btn_MainWindow_Click(object sender, RibbonControlEventArgs e)
        {
            //var ProcDataContext = MultipleDataCopying.GetOrCreateFromBSGlobal();

            //VAService.Current.UIM.ShowDialog<View_Main>(cacheDialog : true);
            VAService.Current.UIM.Show<View_Main>(cacheView : true);

        }

        private void Btn_SavedAnalyses_Click(object sender, RibbonControlEventArgs e)
        {
            //VAService.Current.UIM.ShowDialog<View_Range >(cacheDialog: true);
            //ProgressBar
                
        }

        private void Btn_Test_Click(object sender, RibbonControlEventArgs e)
        {
            VAService.Current.UIM.ShowDialog<View_Test>(cacheDialog: true);
            
        }
    }
}

﻿using System;

namespace ExcelVA.UI
{
    partial class RbnVA : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public RbnVA()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_VA = this.Factory.CreateRibbonTab();
            this.grp_VA1 = this.Factory.CreateRibbonGroup();
            this.btn_MainWindow = this.Factory.CreateRibbonButton();
            this.grp_VA2 = this.Factory.CreateRibbonGroup();
            this.btn_Test = this.Factory.CreateRibbonButton();
            this.tb_VA.SuspendLayout();
            this.grp_VA1.SuspendLayout();
            this.grp_VA2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_VA
            // 
            this.tb_VA.Groups.Add(this.grp_VA1);
            this.tb_VA.Groups.Add(this.grp_VA2);
            this.tb_VA.Label = "VA";
            this.tb_VA.Name = "tb_VA";
            // 
            // grp_VA1
            // 
            this.grp_VA1.Items.Add(this.btn_MainWindow);
            this.grp_VA1.Label = "VA  menu";
            this.grp_VA1.Name = "grp_VA1";
            // 
            // btn_MainWindow
            // 
            this.btn_MainWindow.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_MainWindow.Label = "Main Window";
            this.btn_MainWindow.Name = "btn_MainWindow";
            this.btn_MainWindow.ShowImage = true;
            this.btn_MainWindow.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Btn_MainWindow_Click);
            // 
            // grp_VA2
            // 
            this.grp_VA2.Items.Add(this.btn_Test);
            this.grp_VA2.Label = "Tests";
            this.grp_VA2.Name = "grp_VA2";
            // 
            // btn_Test
            // 
            this.btn_Test.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btn_Test.Label = "Tests";
            this.btn_Test.Name = "btn_Test";
            this.btn_Test.ShowImage = true;
            this.btn_Test.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Btn_Test_Click);
            // 
            // RbnVA
            // 
            this.Name = "RbnVA";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tb_VA);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.RbnVAH_Load);
            this.tb_VA.ResumeLayout(false);
            this.tb_VA.PerformLayout();
            this.grp_VA1.ResumeLayout(false);
            this.grp_VA1.PerformLayout();
            this.grp_VA2.ResumeLayout(false);
            this.grp_VA2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Microsoft.Office.Tools.Ribbon.RibbonTab tb_VA;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grp_VA1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_MainWindow;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grp_VA2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btn_Test;
    }

    partial class ThisRibbonCollection
    {
        internal RbnVA RbnVAH
        {
            get { return this.GetRibbon<RbnVA>(); }
        }

        private T GetRibbon<T>()
        {
            throw new NotImplementedException();
        }
    }
}

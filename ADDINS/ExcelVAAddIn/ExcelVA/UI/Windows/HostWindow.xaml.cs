﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ExcelVA.UI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class HostWindow : Window,  IHostWindow, IDialogHostWindow
    {
        
        public HostWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

        }


        /// <summary>
        /// Hide on CLose event - to make this window available to Cache,
        /// and Use Show() to show it on next time.
        /// </summary>
        public bool HideOnly
        { get; set; }

        public void CloseCancel()
        {
            //DialogResult = false;
            Close();
        }

        public void CloseOK()
        {
            DialogResult = true;
            Close();            
        }

        public void SetChild(UserControl wpfControl)
        {
           MainRegion.Content = wpfControl;
            //cntCtrl_Root.Content = wpfControl;            
        }

        public void SetHeader(string header)
        {
            Title = header;            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (HideOnly)
            {
                e.Cancel = true;
                Hide();
            }
             
            

        }



        /*
         *
         *<ContentControl x:Name="cntCtrl_Root" 
                    HorizontalAlignment="Stretch" 
                    VerticalAlignment="Stretch"
                    HorizontalContentAlignment="Stretch"
                    VerticalContentAlignment="Stretch"
                    >
    </ContentControl>


         <!-- Main Region -->
                        <Border  Margin="5,50,5,5" BorderThickness="0" BorderBrush="{x:Null}" CornerRadius="4,4,4,4"  Canvas.ZIndex="1"  HorizontalAlignment="Stretch" VerticalAlignment="Stretch"  >
                            <ScrollViewer  x:Name="ShellPageScrolleViewer"                                                        
                                 VerticalScrollBarVisibility="Auto" 
                                 HorizontalScrollBarVisibility="Disabled" BorderBrush="{x:Null}"  Style="{StaticResource ScrollViewerStyle}"
                                >
                                <ContentControl x:Name="MainRegion" BorderThickness="0" BorderBrush="{x:Null}"
                                                Regions:RegionManager.RegionName ="MainRegion" 
                                                HorizontalContentAlignment="Stretch" HorizontalAlignment="Stretch"  Margin="0,0,70,0" 
                                                VerticalAlignment="Stretch" VerticalContentAlignment="Stretch" Height="Auto"/>

                            </ScrollViewer>
                        </Border >



         */


    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Threading;
using System.Threading.Tasks;
 
using ExcelVA.Processing;
using ExcelVA.Binds;
using ExcelVA.UI;
using ExcelVA.XL;
using System.IO;
using ExcelVA.Config;
using System.Text.Json;
using ExcelVA.Processing.Check;

namespace ExcelVA.Service
{
    /// <summary>
    ///  Virtual Analyserfor Excel Service  - analising Excel filesbu diffferent CHECKS
    /// </summary>
    public class VAService: IProcessHost, IDisposable
    {
        // TASKS
        // SHOW WINDOWS: 
        // MAIN VIEW WINDOW
        // SELECT RANGE VIEW WINDOW
        // SAVE VIEW WINDOW
        // RUN ANALYZING PROCESS/PAUSE/STOP/CONTINUR
        

        #region ------------ Singleton  Constructor -----------------------
                    
        /// <summary>
        ///   VAHService   Ctor
        /// </summary>
        VAService() { }

        /// <summary>
        /// Singleton Getting Property.
        /// ClassDesc
        /// </summary>     
        public static VAService Current
        {
            get
            {
                return VAHServiceCreator.instance;
            }
        }

        /// <summary>
        /// Creator Class - who instantiating Singleton object 
        /// </summary>
        class VAHServiceCreator
        { 

            /// <summary>
            /// Single target class instance
            /// </summary>
            internal static readonly VAService instance
             = GetService();
             static  VAService GetService()
             {
                    var inst =  new VAService();
                    inst.Initialize();
                    return inst;
             }
        }

        void Initialize()
        {
            UIM = new UIManager<HostWindow>();

            allCatalog = new AggregateCatalog();
            allCatalog.Catalogs.Add(new AssemblyCatalog(typeof(VAService).Assembly));
            iocContainer = new CompositionContainer(allCatalog);

            //Collecting ALL CHECKS 
            //Fill the imports of this object
            try
            {
                Container.ComposeParts(this);// Parts(this);
            }
            catch (CompositionException compositionException)
            {
                Console.WriteLine(compositionException.ToString());
            }           

        }

        #endregion   ----------- Silngleton  Constructor -----------------------


        /// <summary>
        /// UI Manager manages by Views and Windows - instantiating an  caching objects
        /// </summary>
        public UIManager<HostWindow> UIM
        { get; private set; } = null;
               

        #region ------------ MEF Container/Catalog ----------


        /// <summary>
        ///MEF Container
        /// </summary>
        CompositionContainer iocContainer = null;
        
        /// <summary>
        ///  MEF Container 
        /// </summary>
        public CompositionContainer Container
        {
            get { return iocContainer; }            
        }


         AggregateCatalog allCatalog
        {   get;  set;    }

        #endregion  ------------ MEF Container/Catalog ----------
               

        #region  --------------------- CHECKS -------------------
        
        /// <summary>
        /// Менеджеры Команд
        /// </summary>
        [ImportMany(typeof(ICheck), AllowRecomposition = true)]
        public IList<Lazy<ICheck, ICheckMetaData>> checks 
            = new List<Lazy<ICheck, ICheckMetaData>>();
        

        /// <summary>
        /// CHECKS 
        /// </summary>
        public List<ICheck> Checks
        {
            get { return checks.Select(tl => tl.Value).ToList(); }
            set { }
        }


        /// <summary>
        /// Get CHECK
        /// </summary>
        /// <param name="CommandManager"></param>
        /// <returns></returns>
        public ICheck GetCheck(string checkKey)
        {
            try
            {
                return checks.Where(chk => chk.Metadata.Key == checkKey).Select(cmdmngr => cmdmngr.Value).FirstOrDefault();
            }
            catch (Exception)
            {
                throw new Exception("");
            }
        }

        #endregion --------------------- CHECKS -------------------


        #region ------------- LOAD WORKBOOKCHECK|CHECKS --------------

        const string WBCheckFilesDefPath = @"c:\Temp\VAWBookSettings\";

        const string LastSessionFile  = "LastSession.config";
        const string LastSessionFilePath = WBCheckFilesDefPath +LastSessionFile;

        LastSessionCfg LastConfig
        { get; set; } = null;

        static JsonSerializerOptions jsoptions = new JsonSerializerOptions()
        { WriteIndented = true };
        void LoadWBookCheck()
        {
            WorkbookCheck bindWBookCheck = null;

            //1 Folders
            if (Directory.Exists(WBCheckFilesDefPath))
            {
                //Load Last Session Config
                if (File.Exists(LastSessionFilePath))
                {                    
                    var  cfgJsonValue = File.ReadAllText(LastSessionFilePath);
                    LastConfig = JsonSerializer.Deserialize<LastSessionCfg>(cfgJsonValue, jsoptions);
                }

                //Load LAST WBOOK CHECK SETTINGS FILE
                if(LastConfig.IsNotNull() && LastConfig.LastWorkedFile.IsNotNull() 
                    && File.Exists(WBCheckFilesDefPath + LastConfig.LastWorkedFile))
                {
                    var filepath = WBCheckFilesDefPath + LastConfig.LastWorkedFile;
                    bindWBookCheck = JsonSerializer.Deserialize<WorkbookCheck>(filepath, jsoptions);
                }
            }
            if ( Directory.Exists(WBCheckFilesDefPath)== false)
            {
                Directory.CreateDirectory(WBCheckFilesDefPath);
                bindWBookCheck = new WorkbookCheck();
            }


            if (!BSGlobal.ContainsKey(BSGlobal.Key.WBook) )
            {
                BSGlobal.SetValue(BSGlobal.Key.WBook, bindWBookCheck);
            }

        }

         void LoadCheckInfos()
        {
            var checkInfos = new List<CheckInfo>();
            for (int i = 0; i < Checks.Count-1 ; i++)
            {
                checkInfos.Add(Checks[i].GetInfo());
            }

            BSGlobal.SetValue(BSGlobal.Key.CheckInfs, checkInfos);
        }
               
        /// <summary>
        /// Load Binding Store Elements : WorkBookCheck and Check Informations
        /// </summary>
        public void LoadBSElements()
        {
            LoadCheckInfos();
            LoadWBookCheck();
             MultipleDataCopying.GetOrCreateFromBSGlobal();// GetOrCreateFromBS();

        }

        #endregion ------------- LOAD WORKBOOKCHECK|CHECKS --------------


        #region ---------- PROCESS ----------


        public CancellationTokenSource CancelSource
        { get; private set; } = new CancellationTokenSource();
        
        public IBusyTotalControl BusyControl
        { get; private set; }
        public void AttacheBusyControl(IBusyTotalControl busyControl)
        {
            BusyControl = busyControl;
            AttachBItoReporter();
        }


        public ProgressReporter Reporter
        { get; private set; } = new ProgressReporter();

        public void AttachBItoReporter()
        {
            Reporter.ProgressChanged += (sdr, e) =>
            {
                if (BusyControl != null)
                {
                    BusyControl.ChangeProgress(e);
                } 
            };
        }

        public void BI_SayInform(string inform)
        {
            if (BusyControl != null
                && BusyControl.SayInform != null)
            {
                BusyControl.SayInform(inform);
            }
        }

        public void  BI_SayWarning(string warning)
        {
            if (BusyControl != null
                && BusyControl.SayWarning != null)
            {
                BusyControl.SayWarning(warning);
            }            
        }
        public void BI_SayError(string error)
        {
            if (BusyControl != null
                && BusyControl.SayError != null)
            {
                BusyControl.SayError(error);
            }
        }


        public Stack<ProcessStateEn> State
        { get; private set; } = new Stack<ProcessStateEn>(2);

        public Dictionary<string, object> PauseInfo
        { get; private set; } = new Dictionary<string, object>()
           {
             { PIKey.PauseStage, 1 }
            ,{ PIKey.PauseItem, -1 }
        };




        public void StartProc(int stage
            , int startIndex = 0)
        {
            
            var cancelToken = CancelSource.Token;
            var wProcessing = BSGlobal.GetValue(BSGlobal.Key.CurrentProcessing) as IProcess;

            var task = Task.Factory.StartNew(() =>
            {

                // START WORKBOOK
                wProcessing.StartProc(stage
                    , cancelToken
                    ,startIndex
                    ,Reporter);

                // The answer, at last! 
                return 1;
            }, cancelToken)
            .ContinueWith((t) =>
            {
                if (t.IsFaulted)// cancelled: STOPPED | PAUSED
                {

                }
                else if (t.IsCompleted)
                {

                }
            }
            );

        }


        public void StopProc()
        {
            //Console.WriteLine("Операция прервана токеном");
            State.Push(ProcessStateEn.Stopped);
            State.Push(ProcessStateEn.StandBy);
            //cleanining Pausedinfo
            PauseInfo[PIKey.PauseStage] = -1;
            PauseInfo[PIKey.PauseItem] = -1;

            CancelSource.Cancel();

        }

        public void PauseContinueProc()
        {
            //Continuing from paused item
            if (State.Peek() == ProcessStateEn.Paused)
            {
                //contiinue  from 
                var stage = (int)PauseInfo[PIKey.PauseStage];
                var nextItem = (int)PauseInfo[PIKey.PauseItem];

                StartProc(stage, nextItem);

            }
            else // Pausing here
            {
                //Change State
                State.Push(ProcessStateEn.Paused);                                
            }
        }

        public void Dispose()
        {
            UIM.Dispose();
            BSGlobal.Clear();
        }


        #endregion ---------- PROCESS ----------


    }
}

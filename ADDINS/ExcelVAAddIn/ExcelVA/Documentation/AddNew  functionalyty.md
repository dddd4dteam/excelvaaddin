﻿Add new functionality
	New View in Main Window button:
	
		1 Create new View like View_Main_DataCopy and 
			View_Main_DataCopyVM
		2 in View_Mai.xaml add TabItem like
		
 ```xml
<TabItem x:Name="tabItm_DataCopy">
                <TabItem.Header>
                    <StackPanel Orientation="Horizontal">
                        <Ellipse Height="10" Width="10" Fill="Black" />
                        <TextBlock Margin="3"
                            Text="Перенос данных" 
                                   ToolTip="Перенос данных в файл отчета из исходных файлов Excel"
                        />
                    </StackPanel>
                </TabItem.Header>
            </TabItem>
```
        3 in View_Main.cs add TabItem attribute  like
```csharp
[TabItemView("tabItm_DataCopy", typeof(View_Main_DataCopy))]
```



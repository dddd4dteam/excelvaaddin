﻿### Copy data from one workbook to another
 
#### Task : 
        * Two files- workbooks: 
                source - first workbook file
                target - second workbook file
                
        * Column Association rule
            - column header cells. Column in second file should contains the same column  as at the source file.- Its header rows in first and second file.
            
        * One identity column 
            - common column/association column,  to choose wich row from source 
            - first workbook column to target = second workbook column. 
        
        * Row Association rule
            = data in cell of identity column in first file must be the same
            as  data in cell of identity column in second file
           
        * Several data columns
            = in source file - this data must be copy to target column 
            in second file
            
        * Not finded cells  in identity column 
            - non copied data rows  which should be marked or informed about them
        
         variables:
         string OtchetDir 
         
         string SourceWBName
            workbook SourceWB- 
         
         string TargetWBName
            workbook TargetWB - отчет шаблон пустой

         Range SourceHeaders
         Range TargetHeaders
         Cell IdentityColumn - in first WB
         EndCell in IdentityColumn in source

         Range SourceData

 Data Copy Model
![](DataCopy.png)! 
    - Classes can be used to Start Processing
    - Classes can be used to store as JSON Configuration
    - Classes should be used throw the BS- Binding store
    - Clases can notify about Processing Task progress 
    - ? can be used in UI binding directly? 
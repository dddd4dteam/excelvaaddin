﻿#pragma checksum "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "22F2989A70F16C7B6C6B1F32AF357C0EEFF4C9EA9F3729A82CD585E148391CAB"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using ExcelVA.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ExcelVA.Views {
    
    
    /// <summary>
    /// View_Main_Processing
    /// </summary>
    public partial class View_Main_Processing : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_StartCheckingProcess;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_PauseCheckingProcess;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_StopCheckingProcess;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbl_Errors;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lbx_Errors;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar prgbr_CheckingProcess;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lbl_ItemCheckingInfo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ExcelVA;component/ui/views/main/view_main%20processing.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\UI\Views\Main\View_Main Processing.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btn_StartCheckingProcess = ((System.Windows.Controls.Button)(target));
            return;
            case 2:
            this.btn_PauseCheckingProcess = ((System.Windows.Controls.Button)(target));
            return;
            case 3:
            this.btn_StopCheckingProcess = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.lbl_Errors = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.lbx_Errors = ((System.Windows.Controls.ListBox)(target));
            return;
            case 6:
            this.prgbr_CheckingProcess = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 7:
            this.lbl_ItemCheckingInfo = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}


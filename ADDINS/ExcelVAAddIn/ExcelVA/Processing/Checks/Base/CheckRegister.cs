﻿namespace ExcelVA.Processing.Check
{


    //  -Checks:    
    //Checking Formulas:
    // 1 CheckFormulaExistInCell
    // 2 CheckFormulaInCell 
    //Checking Adresses:
    // 3 Check Cell placed in Column
    // 4 Check Cell placed in Row
    // 5 Check Cell placed by Address


    /// <summary>
    /// Cell CHECKS enumeration registry
    /// </summary>
    public class CheckRegister
    {
        /// <summary>
        /// Check target cell value is not [empty] and  the value is formula
        /// </summary>
        public static string FormulaExistInCell = nameof(FormulaExistInCell);

        /// <summary>
        /// Check target cell value is not [empty] and  the value is formula + compare with your formula
        /// </summary>
        public static string FormulaInCell = nameof(FormulaInCell);

        /// <summary>
        /// Check target cell placed in your column
        /// </summary>
        public static string CellPlacedInColumn = nameof(CellPlacedInColumn);

        /// <summary>
        /// Check target cell placed in your row
        /// </summary>
        public static string CellPlacedInRow = nameof(CellPlacedInRow);

        /// <summary>
        /// Check target cell placed by your address
        /// </summary>
        public static string CellPlacedByAddress = nameof(CellPlacedByAddress);
        

    }

}

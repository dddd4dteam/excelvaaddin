﻿ 
using ExcelVA.Processing;
using ExcelVA.Resources;
using ExcelVA.XL.Model;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ExcelVA.Processing.Check
{
    /// <summary>
    /// Base Class for Custom Check algorithms
    /// </summary>    
    public class Check : ICheck
    {
        #region ------------ CTOR ---------------
        public Check() { }

        #endregion------------ CTOR ---------------


        #region ------------ PROPERTIES -------------

        /// <summary>
        /// Check Description
        /// </summary>
         
        public string Description
        { get; protected set; }

        public  virtual CheckInfo GetInfo()
        {
            return null;
        }


        /// <summary>
        /// CHECK TYPE - checking algorithm Key
        /// </summary>
        
        public string Key
        { get; set; }


        /// <summary>
        /// Key: {ID}_{TYPE:int/string/float/bool}
        /// </summary>
         
        public Dictionary<string, string> Params
        { get; set; }


        
        public Dictionary<string, Range> Ranges
        { get; private set; } = new Dictionary<string, Range>();


         
        public Dictionary<string, CellInfo> PrimaryData
        { get; set; } = new Dictionary<string, CellInfo>();

         
        public List<string> Errors
        { get; set; }


        #endregion------------ PROPERTIES ---------------


        public virtual bool CheckCell(int stage, string cellAddress)
        {
            return true;
        }


        #region ----------- IProcess -------------

        public Stack<ProcessStateEn> State
        { get; private set; } = new Stack<ProcessStateEn>(2);

        public Dictionary<string, object> PauseInfo
        { get; private set; } = new Dictionary<string, object>()
            {     { PIKey.PauseItem, -1 }
                , { PIKey.PauseCheckCell,"A1"}
                , { PIKey.PauseStage, 1 }
                , { PIKey.PauseProgress , 0 }
            };





        public virtual void StartProc(int stage
                                , CancellationToken cancelToken
                                , int startIndex = 0
                                , ProgressReporter reporter = null)
        {
            State.Push(ProcessStateEn.Started);
            //report progress  tp UI
            reporter.ItmProgr(1,RC.RPI_StartBeginItem, 3);
            
            //Pause info - save stagr to  
            PauseInfo[PIKey.PauseStage] = stage;

            //CUSTOM  STAGE 1| STAGE 2 
            /*
              for (int i = 0; i < Tasks.Count - 1; i++)
            {
                //Pause info - save next Item to  Pause info
                PauseInfo[PIKey.PauseItem] = i;
                PauseInfo[PIKey.PauseProgress]
                    = (100 / (Tasks.Count - 1)) * (i + 1);

                // Check for cancellation               
                if (cancelToken.IsCancellationRequested)
                {
                    //Console.WriteLine("Операция прервана токеном");
                    State.Push(ProcessStateEn.Stopped);
                    State.Push(ProcessStateEn.StandBy);
                    //cleanining Pausedinfo
                    PauseInfo[PIKey.PauseStage] = -1;
                    PauseInfo[PIKey.PauseItem] = -1;

                    return;
                }

                //AnalyseItem(stage, cancelToken);
            }
             */
             
            State.Push(ProcessStateEn.Completed);
            State.Push(ProcessStateEn.StandBy);
            //report progress  tp UI
            reporter.ItmProgr(77,RC.RPI_StartEndItem, 100);
            Thread.Sleep(1000);

        }


        public virtual void StopProc(ProgressReporter reporter = null)
        {

            //Console.WriteLine("Операция прервана токеном");
            State.Push(ProcessStateEn.Stopped);
            State.Push(ProcessStateEn.StandBy);
            //cleanining Pausedinfo
            PauseInfo[PIKey.PauseStage] = 1;
            PauseInfo[PIKey.PauseCheckCell] = "A1";

            //report progress  tp UI
            reporter.ItmProgr(1, RC.RPI_StoppedItem, 3);
            Thread.Sleep(1000);
            reporter.ItmProgr(1,RC.RPI_ReadyItem , 0);
        }



        public virtual void PauseContinueProc( CancellationToken cancelToken
                                             , ProgressReporter reporter = null)
        {

            //Continuing from paused item
            if (State.Peek() == ProcessStateEn.Paused)
            {
                //contiinue  from 
                var stage = (int)PauseInfo[PIKey.PauseStage];
                var nextItem = (int)PauseInfo[PIKey.PauseItem];

                StartProc(stage, cancelToken, nextItem);

            }
            else // Pausing here
            {
                //Change State
                State.Push(ProcessStateEn.Paused);

                //report progress  tp UI
                reporter.ItmProgr(7, RC.RPI_PausedTotal
                     , (double)PauseInfo[PIKey.PauseProgress]);

            }

        }


        #endregion -----------  IProcess -------------


        #region ----------- RANGE CHEKING ----------


        public void CheckRangeByCols(
             Range range
            , Func<int, int, bool> checkFunc //Col/Row
            , Action<int, int> trueAct     //Col/Row      
            , Action<int, int> falseAct    //Col/Row        
            )
        {

            var colStart = range.From.Adres.Column.Index;
            var colEnd = range.To.Adres.Column.Index;
            var rowStart = range.From.Adres.Row;
            var rowEnd = range.To.Adres.Row;
            for (int i = colStart; i <= colEnd; i++)
            {
                for (int j = rowStart; j < rowEnd; j++)
                {
                    if (checkFunc(i, j))
                    {
                        trueAct(i, j);
                    }
                    else
                    {
                        falseAct(i, j);
                    }
                }
            }
        }


        public void CheckRangeByRows(
            Range range
            , Func<int, int, bool> checkFunc //Col/Row
            , Action<int, int> trueAct     //Col/Row      
            , Action<int, int> falseAct    //Col/Row        
            )
        {
            var colStart = range.From.Adres.Column.Index;
            var colEnd = range.To.Adres.Column.Index;
            var rowStart = range.From.Adres.Row;
            var rowEnd = range.To.Adres.Row;
            for (int i = rowStart; i <= rowEnd; i++)
            {
                for (int j = colStart; j < colEnd; j++)
                {
                    if (checkFunc(j, i))
                    {
                        trueAct(j, i);
                    }
                    else
                    {
                        falseAct(j, i);
                    }
                }
            }
        }




        #endregion   ----------- RANGE CHEKING ---------- 


    }
}





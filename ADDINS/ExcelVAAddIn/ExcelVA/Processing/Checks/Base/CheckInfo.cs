﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Processing.Check
{

    public class CheckInfo
    {

        public static CheckInfo GetCheckInfo(string key
                                            , string description
                                            , string instruction
                                           )
        {
            var checkInf = new CheckInfo();

            checkInf.Key = key;
            checkInf.Description = description;
            checkInf.Instruction = instruction;

            return checkInf;
        }

        public string Key
        { get; private set; }

        public string Description
        { get; private set; }

        public string Instruction
        { get; private set; }

    }

}

﻿using System;
using System.ComponentModel.Composition;

namespace ExcelVA.Processing.Check
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public sealed class CheckAttribute : ExportAttribute
    {

        public CheckAttribute(string key)
            : base(typeof(ICheck))
        {
            Key = key;
        }


        /// <summary>
        /// Key of Check
        /// </summary>
        public string Key
        { get; private set; }

    }

}

﻿ 
using ExcelVA.Processing;
using ExcelVA.XL.Model;

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace ExcelVA.Processing.Check
{

    public interface ICheck: IProcess
    {
        string Key
        { get; }

        /// <summary>
        /// Check Description
        /// </summary>
        string Description
        { get; }

        CheckInfo GetInfo();  

        Dictionary<string, string> Params
        { get; }

        Dictionary<string, Range> Ranges
        { get; }


        Dictionary<string, CellInfo> PrimaryData
        { get; }
        
        List<string> Errors
        { get; }

        bool CheckCell(int stage,string cellAddress);

    }




  
}


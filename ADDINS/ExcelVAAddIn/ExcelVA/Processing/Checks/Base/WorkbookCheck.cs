﻿using ExcelVA.Processing;
using ExcelVA.Resources;
using ExcelVA.UI;
 
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ExcelVA.Processing.Check
{

   
    public class WorkbookCheck:IProcess
    {

        
        public string BaseWBook
        { get; set; }


       
        public string TargetWBook
        { get; set; }


        
        public List<Check> Tasks
        { get; set; }

        #region ------------ IProcess --------------


        public Stack<ProcessStateEn> State
        { get; private set; } = new Stack<ProcessStateEn>(2);

        public Dictionary<string, object> PauseInfo
        { get; private set; } = new Dictionary<string, object>()
            {     { PIKey.PauseItem,-1}
                , { PIKey.PauseStage, 1 }
                , { PIKey.PauseProgress , 0 }
            };



        

        public void StartProc(int stage
                             , CancellationToken cancelToken
                             , int startIndex = 0
                             , ProgressReporter reporter = null)
        {
            State.Push(ProcessStateEn.Started);
            //report progress  tp UI
            reporter.TotProgr(RC.RPI_StartBeginTotal, 3);

            
            //Pause info - save stagr to  
            PauseInfo[PIKey.PauseStage] = stage;

            for (int i = 0; i < Tasks.Count ; i++)
            {
                //Pause info - save next Item to  Pause info
                PauseInfo[PIKey.PauseItem] = i;
                PauseInfo[PIKey.PauseProgress] 
                    = (100 / (Tasks.Count - 1)) * (i + 1);

                // Check for cancellation               
                if (cancelToken.IsCancellationRequested)
                {
                    //Console.WriteLine("Операция прервана токеном");
                    State.Push(ProcessStateEn.Stopped);
                    State.Push(ProcessStateEn.StandBy);
                    //cleanining Pausedinfo
                    PauseInfo[PIKey.PauseStage] = -1;
                    PauseInfo[PIKey.PauseItem] = -1;
                    
                    return;
                }

                Tasks[i].StartProc(stage, cancelToken);
            }

            State.Push(ProcessStateEn.Completed);
            State.Push(ProcessStateEn.StandBy);
            //report progress  tp UI
            reporter.TotProgr(RC.RPI_StartEndTotal, 100);
            Thread.Sleep(5000);
            reporter.TotProgr(RC.RPI_ReadyTotal, 0);
        }

        public void StopProc(ProgressReporter reporter = null)
        {            
            //Console.WriteLine("Операция прервана токеном");
            State.Push(ProcessStateEn.Stopped);
            State.Push(ProcessStateEn.StandBy);
            //cleanining Pausedinfo
            PauseInfo[PIKey.PauseStage] = -1;
            PauseInfo[PIKey.PauseItem] = -1;

            //report progress  tp UI
            reporter.TotProgr(RC.RPI_StoppedTotal, 3);
            Thread.Sleep(3000);
            reporter.TotProgr(RC.RPI_ReadyTotal, 0);
        }

        public void PauseContinueProc( CancellationToken cancelToken
                                     , ProgressReporter reporter = null)
        {
            //Continuing from paused item
            if (State.Peek() == ProcessStateEn.Paused)
            {               
                //contiinue  from 
                var stage = (int)PauseInfo[PIKey.PauseStage];
                var nextItem = (int)PauseInfo[PIKey.PauseItem];

                StartProc(stage, cancelToken, nextItem);
               
            }
            else // Pausing here
            {
                //Change State
                State.Push(ProcessStateEn.Paused);
                
                //report progress  tp UI
                reporter.TotProgr(RC.RPI_PausedTotal
                     ,(double)PauseInfo[PIKey.PauseProgress]);

            }
        }
         
        #endregion ------------ IProcess --------------

    }


}

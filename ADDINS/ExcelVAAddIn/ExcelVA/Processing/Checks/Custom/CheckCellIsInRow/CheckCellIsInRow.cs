﻿using ExcelVA.Processing;
using ExcelVA.Resources;
 
using System.Threading;

namespace ExcelVA.Processing.Check.Custom
{

    
    [Check(nameof(CheckCellIsInRow))]
    public class CheckCellIsInRow : Check
    {



        CheckCellIsInRow()
        {
            Key = GetType().Name;
            Description = RC.CHKINF_CellIsInRow_Desc;
        }

        public override CheckInfo GetInfo()
        {
            var inf = CheckInfo.GetCheckInfo(
                 Key
                , Description
                , RC.CHKINF_CellIsInRow_Instruct
                );

            return inf;
        }

        //STAGE 1
        //STAGE 2




        //STAGE 1 : collecting adresses of Header regions cells  
        //STAGE 2   comparing  header region with taarged workbook cells

        void StageOne(CancellationToken cancelToken
                      , int startIndex = 0
                      , ProgressReporter reporter = null)
        {
            State.Push(ProcessStateEn.Started);
            //report progress  tp UI
            reporter.ItmProgr(1, RC.RPI_StartBeginItem, 3);

            //Pause info - save stagr to  
            PauseInfo[PIKey.PauseStage] = 1;

            //CUSTOM  STAGE 1| STAGE 2 
            /*
              for (int i = 0; i < Tasks.Count - 1; i++)
            {
                //Pause info - save next Item to  Pause info
                PauseInfo[PIKey.PauseItem] = i;
                PauseInfo[PIKey.PauseProgress]
                    = (100 / (Tasks.Count - 1)) * (i + 1);

                // Check for cancellation               
                if (cancelToken.IsCancellationRequested)
                {
                    //Console.WriteLine("Операция прервана токеном");
                    State.Push(ProcessStateEn.Stopped);
                    State.Push(ProcessStateEn.StandBy);
                    //cleanining Pausedinfo
                    PauseInfo[PIKey.PauseStage] = -1;
                    PauseInfo[PIKey.PauseItem] = -1;

                    return;
                }

                //CheckCell ( stage, address);
            }
             */


            State.Push(ProcessStateEn.Completed);
            State.Push(ProcessStateEn.StandBy);
            //report progress  tp UI
            reporter.ItmProgr(77, RC.RPI_StartEndItem, 100);
            Thread.Sleep(1000);

        }

        void StageTwo(CancellationToken cancelToken
                      , int startIndex = 0
                      , ProgressReporter reporter = null)
        {

        }




        public override void StartProc(int stage
                               , CancellationToken cancelToken
                               , int startIndex = 0
                               , ProgressReporter reporter = null)
        {
            if (stage == 1)
            {
                StageOne(cancelToken, startIndex, reporter);
            }
            else if (stage == 2)
            {
                StageTwo(cancelToken, startIndex, reporter);
            } 
        } 


        public override bool CheckCell(int stage, string cellAddress)
        {
            return true;
        } 

    }


}

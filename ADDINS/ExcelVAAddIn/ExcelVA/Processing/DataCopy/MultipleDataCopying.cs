﻿ 
using ExcelVA.Binds;
using ExcelVA.Processing;
using ExcelVA.Resources;
using ExcelVA.UI;
using ExcelVA.XL.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace ExcelVA.Processing
{

    
    /// <summary>
    /// Super Processing task - copy data from several source files 
    /// to target-otchet files. 
    /// </summary>
    public class MultipleDataCopying : IProcess, INotifyPropertyChanged
    {
       public const string InitialDir = @"c:\Temp\VAWBookSettings\DataCopy";
       public const string CofigFileNameDef = "dataCopyConfig";
       public const string CofigFileExt = ".jdtcpconf";
       public const string CofigFileFilter = "Data Copy Config File (*.jdtcpconf)|*.jdtcpconf";
         
        
       public const string Param_OpenTaskIndex = nameof(Param_OpenTaskIndex);

        #region -----------  DATA CONTEXT IN BS GET/CHANGE -------------- 

        public const string DataCopyDataContextKey = nameof(DataCopyDataContextKey);

        public static MultipleDataCopying GetOrCreateFromBSGlobal()
        {
            if (BSGlobal.GetValue(DataCopyDataContextKey) ==  null)
            {
                BSGlobal.SetValue(DataCopyDataContextKey, new MultipleDataCopying());
            }
            return BSGlobal.GetValue(DataCopyDataContextKey) as MultipleDataCopying;
        }

        public static void ChangeInBSGlobal(MultipleDataCopying copyContextInfo)
        {
           BSGlobal.SetValue(DataCopyDataContextKey, copyContextInfo); 
        }


        //public static MultipleDataCopying GetOrCreateFromBS()
        //{             
        //    if (BS.Store.ContainsKey(DataCopyDataContextKey) == false)
        //    {
        //        BS.Store.Add(DataCopyDataContextKey, new MultipleDataCopying());
        //    }
             
        //    return BS.Store[DataCopyDataContextKey] as MultipleDataCopying;
        //}
        //public static void ChangeContextInBS(MultipleDataCopying copyContextInfo)
        //{
        //    if (BS.Store.ContainsKey(DataCopyDataContextKey) == false)
        //    {
        //        BS.Store.Add(DataCopyDataContextKey, copyContextInfo);
        //    }
        //    else
        //    {
        //        BS.Store[DataCopyDataContextKey] = copyContextInfo;
        //    }
        //}

        #endregion-----------  DATA CONTEXT IN BS GET/CHANGE --------------

        #region -------- DATA COPY CONFIG FILE -------------


        #region ------------------NOTIFIABLE MODEL ITEM: DataCopyConfigFilePath ------------------------------
 
        string _DataCopyConfigFilePath  = "";

        /// <summary>
        /// Description
        /// </summary>
        public string DataCopyConfigFilePath 
        {
            get { return _DataCopyConfigFilePath ; }
            set
            {
                _DataCopyConfigFilePath  = value; //Field changing
                OnPropertyChanged(() => DataCopyConfigFilePath );
                if (string.IsNullOrEmpty(value) == false)
                {
                    DataCopyConfigFile = new FileInfo(value);
                }
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: DataCopyConfigFilePath ------------------------------


        /// <summary>
        /// MultipleDataCopying json config file info.
        /// </summary>
        [JsonIgnore]
        public FileInfo DataCopyConfigFile
        { get; set; }

        [JsonIgnore]
        public string DataCopyConfigFileDir
        {
            get
            {
                if (DataCopyConfigFile != null && DataCopyConfigFile.Directory != null)
                {
                    return DataCopyConfigFile.Directory.FullName;
                }
                else return null;                 
            }
        }

        [JsonIgnore]
        public bool IsDataCopyConfigFileSaved
        { get; set; }

        #endregion -------- DATA COPY CONFIG FILE -------------

        #region ---------- OTCHET FILE ------------

        #region ------------------NOTIFIABLE MODEL ITEM: TargetOtchetFilePath ------------------------------
        // serializable property    
        string _TargetOtchetFilePath  = "";

        /// <summary>
        /// Description TargetOtchetFilePath 
        /// </summary>
        public string TargetOtchetFilePath 
        {
            get { return _TargetOtchetFilePath; }
            set
            {
                _TargetOtchetFilePath  = value; //Field changing
                OnPropertyChanged(() => TargetOtchetFilePath );
                if (string.IsNullOrEmpty(value) == false)
                {
                    TargetOtchetFile = new FileInfo(value);
                }
            }
        }


        #endregion ------------------NOTIFIABLE MODEL ITEM: TargetOtchetFile ------------------------------


        /// <summary>
        /// Target-otchet xlsx file 
        /// </summary>
        [JsonIgnore]
        public FileInfo TargetOtchetFile
        { get; set; }

        [JsonIgnore]
        public string TargetOtchetFileDir
        { get 
            {
                if (TargetOtchetFile != null && TargetOtchetFile.Directory != null)
                {
                    return TargetOtchetFile.Directory.FullName;
                }
                else return null;                  
            }
        }

        #endregion ---------- OTCHET FILE ------------


        #region ----------- IDENTITY COLUMN RANGE ----------
        
        #region ------------------NOTIFIABLE MODEL ITEM:  TargetIdentityDataRange ------------------------------

        Range _TargetIdentityDataRange =   Range.FirstRange;

        /// <summary>
        /// Target Identity Data Range by Identity Column
        /// </summary>
        public Range TargetIdentityDataRange
        {
            get { return _TargetIdentityDataRange; }
            set
            {
                _TargetIdentityDataRange = value; //Field changing
                OnPropertyChanged(() => TargetIdentityDataRange);
                TargetStartRow = value.From.Adres.Row;
                TargetEndRow = value.To.Adres.Row;
                 
            }
        }

        public int TargetStartRow = -1;
        public int TargetEndRow = -1;
        #endregion ------------------NOTIFIABLE MODEL ITEM: TargetIdentityDataRange------------------------------

        #endregion ----------- IDENTITY COLUMN / DATA RANGE -----------


        #region ----------   DATA COPY TASKS ------------

        #region ------------------NOTIFIABLE MODEL ITEM: SelectedTaskIndex ---------------------------

        int _SelectedTaskIndex = new int();

        /// <summary>
        /// Selected task Index
        /// </summary>
        public int SelectedTaskIndex
        {
            get { return _SelectedTaskIndex; }
            set
            {
                _SelectedTaskIndex = value; //Field changing
                OnPropertyChanged(() => SelectedTaskIndex);
                SelectedTask = GetSelectedTask();
            }
        }
    
        #endregion ------------------NOTIFIABLE MODEL ITEM: SelectedTaskIndex ------------------------------


        #region ------------------NOTIFIABLE MODEL ITEM: SelectedTask ------------------------------
         
        DataCopying  _SelectedTask  = new DataCopying ();

        /// <summary>
        /// Description
        /// </summary>
        public DataCopying  SelectedTask 
        {
            get { return _SelectedTask ; }
            set
            {
                _SelectedTask  = value; //Field changing
                OnPropertyChanged(() => SelectedTask );
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: SelectedTask ------------------------------


        public ObservableCollection<DataCopying> CopyTasks
        { get; set; } = new ObservableCollection<DataCopying>();

        /// <summary>
        /// Create New DataCopying Task 
        /// and set SelectedTaskIndex to its index.
        /// </summary>
        /// <returns></returns>
        public int CreateNewTask()
        {
            CopyTasks.Add(new DataCopying());
            SelectedTaskIndex = CopyTasks.Count - 1;
            return SelectedTaskIndex;
        }

        public DataCopying GetSelectedTask()
        {
            return CopyTasks[SelectedTaskIndex];
        }

        #endregion  ----------   DATA COPY TASKS ------------


        #region ------------ SAVE/ LOAD  AS  JSON -------------

        public static void SaveAsJson(MultipleDataCopying processingObj)
        {
            if (processingObj.DataCopyConfigFile == null) return;
            var stringtoSave = JsonSerializer.Serialize<MultipleDataCopying>(processingObj);
            
            File.WriteAllText(processingObj.DataCopyConfigFile.FullName, stringtoSave); 
        }

        public static void LoadFromJson(MultipleDataCopying processingObj )
        {
            if (processingObj.DataCopyConfigFile.Exists == false) return ;
            var stringtoLoad = File.ReadAllText(processingObj.DataCopyConfigFile.FullName);
            
           var resultObj= JsonSerializer.Deserialize<MultipleDataCopying>(stringtoLoad);

            BSGlobal.SetValue(DataCopyDataContextKey, resultObj);
             
        }

        #endregion ------------ SAVE/ LOAD  AS  JSON -------------




        #region ------------ IProcess --------------


        public Stack<ProcessStateEn> State
        { get; private set; } = new Stack<ProcessStateEn>(2);

        public Dictionary<string, object> PauseInfo
        { get; private set; } = new Dictionary<string, object>()
            {     { PIKey.PauseItem,-1}
                , { PIKey.PauseStage, 1 }
                , { PIKey.PauseProgress , 0 }
            };

      
        public void StartProc(int stage
                             , CancellationToken cancelToken
                             , int startIndex = 0
                             , ProgressReporter reporter = null)
        {
            State.Push(ProcessStateEn.Started);
            //report progress  tp UI
            reporter.TotProgr(RC.RPI_StartBeginTotal, 3);

            
            //Pause info - save stagr to  
            PauseInfo[PIKey.PauseStage] = stage;

            for (int i = 0; i < CopyTasks.Count ; i++)
            {
                //Pause info - save next Item to  Pause info
                PauseInfo[PIKey.PauseItem] = i;
                PauseInfo[PIKey.PauseProgress] 
                    = (100 / (CopyTasks.Count)) * (i + 1);//  Count - 1

                // Check for cancellation               
                if (cancelToken.IsCancellationRequested)
                {
                    //Console.WriteLine("Операция прервана токеном");
                    State.Push(ProcessStateEn.Stopped);
                    State.Push(ProcessStateEn.StandBy);
                    //cleanining Pausedinfo
                    PauseInfo[PIKey.PauseStage] = -1;
                    PauseInfo[PIKey.PauseItem] = -1;
                    
                    return;
                }

                CopyTasks[i].StartProc(stage,  cancelToken , reporter);
            }

            State.Push(ProcessStateEn.Completed);
            State.Push(ProcessStateEn.StandBy);
            //report progress  tp UI
            reporter.TotProgr(RC.RPI_StartEndTotal, 100);
            Thread.Sleep(5000);// work emulation
            reporter.TotProgr(RC.RPI_ReadyTotal, 0);
        }

        public void StopProc(ProgressReporter reporter = null)
        {            
            //Console.WriteLine("Операция прервана токеном");
            State.Push(ProcessStateEn.Stopped);
            State.Push(ProcessStateEn.StandBy);
            //cleanining Pausedinfo
            PauseInfo[PIKey.PauseStage] = -1;
            PauseInfo[PIKey.PauseItem] = -1;

            //report progress  tp UI
            reporter.TotProgr(RC.RPI_StoppedTotal, 3);
            Thread.Sleep(3000);// work emulation
            reporter.TotProgr(RC.RPI_ReadyTotal, 0);
        }

        public void PauseContinueProc( CancellationToken cancelToken
                                     , ProgressReporter reporter = null)
        {
            //Continuing from paused item
            if (State.Peek() == ProcessStateEn.Paused)
            {               
                //contiinue  from 
                var stage = (int)PauseInfo[PIKey.PauseStage];
                var nextItem = (int)PauseInfo[PIKey.PauseItem];

                StartProc(stage, cancelToken, nextItem);
               
            }
            else // Pausing here
            {
                //Change State
                State.Push(ProcessStateEn.Paused);
                
                //report progress  tp UI
                reporter.TotProgr(RC.RPI_PausedTotal
                     ,(double)PauseInfo[PIKey.PauseProgress]);

            }
        }

        #endregion ------------ IProcess --------------

        #region ----------- INotifyPropertyChanged ---------------

        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Raise Property changed event handler.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        public virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }


        /// <summary>
        ///Raise Property changed event handler.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(String propertyName)
        {
            //var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion ----------- INotifyPropertyChanged ---------------

    }


}

﻿
using ExcelVA.Views;
using ExcelVA.XL;
using ExcelVA.XL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.Json.Serialization;
using System.Threading;

namespace ExcelVA.Processing
{
    /// <summary>
    ///  Item Processing task - copy data from ONE Source File 
    ///  to Target file. 
    /// </summary>
    public class DataCopying : INotifyPropertyChanged
    {
        public DataCopying()
        {
            Description.Add(" New Data Copy Task");
        }

        #region ----------- INotifyPropertyChanged ---------------

        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Raise Property changed event handler.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        public virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }


        /// <summary>
        ///Raise Property changed event handler.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(String propertyName)
        {
            //var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion ----------- INotifyPropertyChanged ---------------


        #region -------- KEY /DESCRIPTION ------------
        
        public string Key
        { get; set; }
    

        #region ------------------NOTIFIABLE MODEL ITEM: Description ------------------------------
        
        List<string>  _Description  = new List<string> ();

        /// <summary>
        /// Description  of DataCopying task 
        /// </summary>
        public List<string>  Description 
        {
            get { return _Description ; }
            set
            {
                _Description  = value; //Field changing
                OnPropertyChanged(() => Description );
            }
        }


        #endregion ------------------NOTIFIABLE MODEL ITEM: Description ------------------------------

        public override string ToString()
        {
            var mergedDescript = "";
            for (int i = 0; i < Description.Count; i++)
            {
                mergedDescript += " " + Description[i];
            }
            return mergedDescript;
        }

        #endregion-------- KEY /DESCRIPTION ------------


        #region -------------- SOURCE FILE --------------

        /// <summary>
        /// Target-otchet xlsx file 
        /// </summary>
        [JsonIgnore]
        public FileInfo SourceFile
        { get; set; }


        #region ------------------NOTIFIABLE MODEL ITEM: SourceFilePath------------------------------

        string _SourceFilePath = "";

        /// <summary>
        /// Description
        /// </summary>
        public string SourceFilePath
        {
            get { return _SourceFilePath; }
            set
            {
                _SourceFilePath = value; //Field changing
                OnPropertyChanged(() => SourceFilePath);

                if (string.IsNullOrEmpty(value) == false)
                {
                    SourceFile = new FileInfo(value);
                    Description.Clear();
                    Description.Add(SourceFile.Name);
                }
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: SourceFile Path------------------------------


        #endregion-------------- SOURCE FILE --------------


        #region -------------- WORKBOOKS/ WORKSHEETS------------ 



        ///// <summary>
        ///// Otchet- Target WorkBook File Name
        ///// </summary>
        //public string TargetWBName
        //{ get; set; }


        ///// <summary>
        /////  Source WorkBook File Name
        ///// </summary>
        //public string SourceWBName
        //{ get; set; }

        /// <summary>
        /// Source WorkBook's Sheet Name
        /// </summary>
        public string SourceSheetName
        { get; set; }

        //public int SourceSheetIndex
        //{ get; set; }


        ///// <summary>
        ///// Otchet- Target WorkBook's Sheet Name
        ///// </summary>
        //public string TargetSheetName
        //{ get; set; }
        //public int TargetSheetIndex
        //{ get; set; }

        #endregion -------------- WORKBOOKS/ WORKSHEETS------------ 


        #region ---------- IDENTITY RANGE/ FILTER -------------


        #region ------------------NOTIFIABLE MODEL ITEM: SourceIdentityDataRange ------------------------------
         
        Range  _SourceIdentityDataRange  =  Range.FirstRange;

        /// <summary>
        ///  Source data range   in Identity Column in Source WB 
        /// </summary>
        public Range  SourceIdentityDataRange 
        {
            get { return _SourceIdentityDataRange ; }
            set
            {
                _SourceIdentityDataRange  = value; //Field changing
                OnPropertyChanged(() => SourceIdentityDataRange );
                SourceStartRow = value.From.Adres.Row;
                SourceEndRow = value.To.Adres.Row;

            }
        }
        public int SourceStartRow = -1;
        public int SourceEndRow = -1;
       
        #endregion ------------------NOTIFIABLE MODEL ITEM: SourceIdentityDataRange ------------------------------


        /// <summary>
        /// Row Filter - by data in additional - filter column. 
        /// </summary>
        public RowFilter RFilter
        { get; set; } = new RowFilter();


        #endregion   ---------- IDENTITY  RANGE / FILTER -------------



        #region ----------- DATA COLUMNS MAP ---------------
      

        #region ------------------NOTIFIABLE MODEL ITEM: DataColumnsStr ------------------------------
         
        string  _DataColumnsStr  = "(1)!A-(1)!A";

        /// <summary>
        ///  Data Columns as String for Text Box binding.
        /// </summary>
        public string  DataColumnsStr 
        {
            get { return _DataColumnsStr ; }
            set
            {
                _DataColumnsStr  = value; //Field changing
                OnPropertyChanged(() => DataColumnsStr );
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: DataColumnsStr ------------------------------


        /// <summary>
        /// Data Columns Map :   [ Source Column Cell - Target Column Cell,... ]
        /// Ex:  [(WsInd)!S-(WsInd)!T; (WsInd)!SX-(WsInd)!AT, ...] 
        /// </summary>
        public List<KeyValuePair<Cell, Cell>> DataColumnsMap
        { get; set; } = new List<KeyValuePair<Cell, Cell>>();


        #endregion ----------- DATA COLUMNS  MAP ---------------



        MultipleDataCopying Processing
        { 
          get {
                 return MultipleDataCopying.GetOrCreateFromBSGlobal();
              }
        }



        /// <summary>
        /// Rows Copy(Founded in Source) 
        ///  result of rows in Identity Column in Target WorkBook's TargetSheet 
        /// </summary>
        public Dictionary<int, bool> CopyResultsByRows
        { get; set; } = new Dictionary<int, bool>();


        #region ----------- PROCESSING METHODS -------------
                 

        List<Cell> TargetIdentityRangeOfCells 
            = new List<Cell>();
       

        List<Cell> SourceIdentityRangeOfCells 
            = new List<Cell>();
        


        List<Cell> SourceFiltersRangeOfCells // source filter column data
            = new List<Cell>();

        
        List<Cell> BufferRangeOfCells        // source filter column data
            = new List<Cell>();


        internal void StartProc(int stage
                                , CancellationToken cancelToken
                                , ProgressReporter reporter )
        {   // Copy Data from Source WBook to Target
             
            InitWBsAndSheets( );
            
            CheckParamsInited( );

            // The Copy Itselves
            CopyDataWithInitedParams(  reporter, cancelToken);

            ClearWBsAndSheets( );
        }

     

        private void InitWBsAndSheets( )
        {
            if(ExcelApp.MainBook == null
             && ExcelApp.MainBook.Name != Processing.TargetOtchetFile.Name)
            {  ExcelApp.LoadMainWB(Processing.TargetOtchetFile.FullName);
              //select sheet
              var sheetInMainWB =  Processing.TargetIdentityDataRange.From.Adres.Sheet;
              ExcelApp.SelectMainSheet(sheetInMainWB);
            }

            // Back WB and Sheet
            ExcelApp.LoadBackWB( this.SourceFile.FullName);
            // select sheet    
            var sheetInBackWB = this.SourceIdentityDataRange.From.Adres.Sheet;
            ExcelApp.SelectBackSheet(sheetInBackWB);

            // load identity Columns
            SourceIdentityRangeOfCells = SourceIdentityDataRange.CollectCells();
            foreach (var cellItem in SourceIdentityRangeOfCells)
            { cellItem.GetBackSheetCellVal(); }

            TargetIdentityRangeOfCells = Processing.TargetIdentityDataRange.CollectCells();
            foreach (var cellItem in TargetIdentityRangeOfCells)
            { cellItem.GetMainSheetCellVal(); }

            // load filters column
            var filtersRangeSrc =   Range.RangeInColumn
                ( RFilter.SourceFilterColumnCell.Adres.Sheet
                , RFilter.SourceFilterColumnCell.Adres.Column
                , SourceIdentityDataRange.From.Adres.Row
                , SourceIdentityDataRange.To.Adres.Row);
             
            //lod cell Values
            SourceFiltersRangeOfCells = filtersRangeSrc.CollectCells();
            foreach (var cellItem in SourceFiltersRangeOfCells)
            {  cellItem.GetBackSheetCellVal();  }
        
        }
        private void CheckParamsInited( )
        {
            // TO DO 
        }

        private void CopyDataWithInitedParams(  ProgressReporter reporter
                                              , CancellationToken cancelToken)
        {
            // for each mapItem of S-T columns  
            // for each cell  from source data column
            //  check source filter in such row 
            //  find target sheet row - by identity column Values   
            //  set to target Column and Row index Cell Value
            foreach (var mapItem in  DataColumnsMap )
            {
                var srcColumnCell = mapItem.Key;
                var srcDataRange = Range.RangeInColumn
                    ( srcColumnCell.Adres.Sheet
                    , srcColumnCell.Adres.Column
                    , SourceIdentityDataRange.From.Adres.Row
                    , SourceIdentityDataRange.To.Adres.Row
                    );
                BufferRangeOfCells = srcDataRange.CollectCells();
                foreach (var cellItem in BufferRangeOfCells)
                { cellItem.GetBackSheetCellVal(); }

                // perenos
                for (int i = 0; i < BufferRangeOfCells.Count; i++)
                {   
                    var dataCell = BufferRangeOfCells[i];
                    if ( string.IsNullOrEmpty(dataCell.Text) )
                    {   // ERROR: say data is Null error
                        var errorReportInf = ProgressInfo.NewError( $"Значение NULL_OR_EMPTY в Ячейке[{dataCell.Adres.Value}] с данными в Исходном файле[{SourceFile.Name}] ");
                        reporter.Report(errorReportInf);
                        continue;
                    }

                    //  check source filter in such row 
                    var filterCell = SourceFiltersRangeOfCells[i];
                    var identitySrcCell = SourceIdentityRangeOfCells[i];

                    if (RFilter.FilterValue != filterCell.Text )
                    {   // WARNING: say filter value ignore current row 
                        var warningReportInf = ProgressInfo.NewWarning($"Значение в Ячейке[{dataCell.Adres.Value}] игнорировано по значению фильтра  [{RFilter.FilterValue}] ");
                        reporter.Report(warningReportInf);
                        continue;
                    }

                    //  find target sheet row - by identity column Values   
                    var trgIdentCell = GetCellWithEqualValue(TargetIdentityRangeOfCells, identitySrcCell);
                    if (trgIdentCell == null)
                    {  // say Error - Целевая строка не найдена - по Идентити 
                       var errorReportInf = ProgressInfo.NewError($" Не найдена в Целевом фвйле строка для втавки по Идентификатору[{identitySrcCell.Text}] из исходного файла  ");
                       reporter.Report(errorReportInf);

                       continue; 
                    }
                    var trgSheetRow = trgIdentCell.Adres.Row;

                    //  set to target Column and Row index Cell Value
                    var targDataCell = new Cell( trgSheetRow
                                                , mapItem.Value.Adres.Column.Val 
                                                , trgIdentCell.Adres.Sheet);
                    targDataCell.Text = dataCell.Text;
                    targDataCell.SetMainSheetCellVal();
                } 

            }

            // INFORM: say filter value ignore current row 
            var informReportInf = ProgressInfo.NewInform($"  УСПЕШНО Завершен Перенос для всех столбцов-связок файла[{SourceFile.Name}] ");
            reporter.Report(informReportInf);

        }

        private Cell GetCellWithEqualValue(List<Cell> targetIdentityRangeOfCells, Cell identitySrcCell)
        {
            foreach (var cellItem in TargetIdentityRangeOfCells)
            {
                if (cellItem.Text == identitySrcCell.Text)
                {    return cellItem;   }  
            }
            return null;
        }

        private void ClearWBsAndSheets( )
        {
            // TO DO 

        }

       

        #endregion ----------- PROCESSING METHODS -------------

    }

}

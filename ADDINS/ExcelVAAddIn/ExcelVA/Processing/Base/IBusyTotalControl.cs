﻿


using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;

namespace ExcelVA.Processing
{
    public interface IBusyTotalControl: IBusyItemControl
    {        
        void LoadCheckItems();

        System.Action<string> SayInform
        { get; set; }
        System.Action<string> SayWarning 
        { get; set; } 

        System.Action<string> SayError
        { get; set; }

    }
}

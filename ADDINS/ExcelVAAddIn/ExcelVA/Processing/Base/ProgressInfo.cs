﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Processing
{
       

    public class PIKey
    {
        public const string Inform = nameof(Inform);
        public const string Warning = nameof(Warning);
        public const string Error = nameof(Error);


        public const string ItemIndex = nameof(ItemIndex);
        public const string TotalProgress = nameof(TotalProgress);
        public const string ItemProgress = nameof(ItemProgress);        
       
        public const string TotalTitle = nameof(TotalTitle);
        public const string ItemTitle = nameof(ItemTitle);

        public const string TotalStage = nameof(TotalStage);
        public const string ItemStage = nameof(ItemStage);

        public const string PauseItem = nameof(PauseItem);
        public const string PauseStage = nameof(PauseStage);
        public const string PauseCheckCell = nameof(PauseCheckCell);
        public const string PauseProgress = nameof(PauseProgress);
    
    }


    /// <summary>
    ///  Progress info 
    /// </summary>    
    public partial struct ProgressInfo
    {
        public ProgressInfo(bool useDefalult = true)
        {
            Items = new Dictionary<string, object>();
        }
                              
        public Dictionary<string, object> Items
        { get; private set; }

        public static ProgressInfo NewInform(string inform)
        {
            var progres = new ProgressInfo(true);
            progres.Items.Add(PIKey.Inform , inform);
            return progres;
        }

        public static ProgressInfo NewError(string error)
        {
            var progres = new ProgressInfo(true);
            progres.Items.Add(PIKey.Error, error);

            return progres;
        }
        public static ProgressInfo NewWarning(string warning)
        {
            var progres = new ProgressInfo(true);
            progres.Items.Add(PIKey.Warning, warning);

            return progres;
        }


        public static ProgressInfo NewTotalProgress(string title, double percent)
        {
            var progres = new ProgressInfo(true);
            progres.Items.Add(PIKey.TotalTitle, title);
            progres.Items.Add(PIKey.TotalProgress, percent);            
            return progres;
        }

        public static ProgressInfo NewItemProgress(int index, string title, double percent)
        {
            var progres = new ProgressInfo(true);
            progres.Items.Add(PIKey.ItemIndex, index);
            progres.Items.Add(PIKey.ItemTitle, title);
            progres.Items.Add(PIKey.ItemProgress, percent);

            return progres;
        }

    }




}

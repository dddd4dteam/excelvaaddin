﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ExcelVA.Processing.Base
{

    public class ProgresBarItem : ProgressBar, IBusyItemControl
    {
        public void ChangeProgress(ProgressInfo progres)
        {
            if (progres.Items.ContainsKey(PIKey.ItemProgress))
            {
                var itemProgress = progres.Items[PIKey.ItemProgress];
                this.Value = (double)itemProgress;
            } 
        }
    }


    public class ProgresBarTotal : ProgressBar, IBusyTotalControl
    {
        public Action<string> SayWarning
        { get; set; }        
        public Action<string> SayError
        { get; set; }
        public Action<string> SayInform
        { get; set; }
        public void ChangeProgress(ProgressInfo progres)
        {
            if (progres.Items.ContainsKey(PIKey.Error ))
            {
                var error = progres.Items[PIKey.Error] as string;
                SayError(error);
            }
            if (progres.Items.ContainsKey(PIKey.Inform))
            {
                var inform = progres.Items[PIKey.Inform] as string;
                SayInform(inform);
            }
            if (progres.Items.ContainsKey( PIKey.Warning ))
            {
                var warning = progres.Items[PIKey.Warning] as string;
                SayWarning(warning);
            }


            // for Total Progress info
            if (progres.Items.ContainsKey(PIKey.TotalTitle) )
            {
                var totTitle = progres.Items[PIKey.TotalTitle];
            }
            
            if (progres.Items.ContainsKey(PIKey.TotalProgress))
            {
                var totalProgress = progres.Items[PIKey.TotalProgress];
                this.Value = (double)totalProgress;
            }  
        }

        public void LoadCheckItems()
        {

        }

    }

}

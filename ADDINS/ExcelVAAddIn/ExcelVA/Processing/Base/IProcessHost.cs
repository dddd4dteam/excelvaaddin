﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace ExcelVA.Processing
{


    /// <summary>
    ///  ProcessHost- looks like IProcess but 
    ///   1- TPL Tasks should be realized here
    ///   2-IBusyIndicator and ProgressReporter aspects also belong to it
    /// </summary>
    public interface IProcessHost
    {

        IBusyTotalControl BusyControl
        { get; }

        void AttacheBusyControl(IBusyTotalControl busyControl);

        ProgressReporter Reporter
        { get; }

        void AttachBItoReporter();


        CancellationTokenSource CancelSource
        { get; }


        Stack<ProcessStateEn> State { get; }

        Dictionary<string, object> PauseInfo
        { get; }

        /// <summary>
        /// Start new Process
        /// </summary>
        void StartProc(int stage           
            , int startIndex = 0);

        /// <summary>
        /// Stop on cancel Process
        /// </summary>
        void StopProc();

        /// <summary>
        /// Pause/Continue if paused - starting Process 
        /// </summary>
        void PauseContinueProc();



    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace ExcelVA.Processing
{
    public enum ProcessStateEn
    {
        StandBy = 2
       ,Started  = 4
       ,Paused  =  8
       ,Stopped = 16
       ,Completed = 32
    }




    public interface IProcess
    {

        Stack<ProcessStateEn> State { get; }

        Dictionary<string,object> PauseInfo
        { get; }

        /// <summary>
        /// Start new Process
        /// </summary>
        void StartProc( int stage, 
            CancellationToken cancelToken
            , int startIndex = 0
            , ProgressReporter reporter = null);

        /// <summary>
        /// Stop on cancel Process
        /// </summary>
        void StopProc(ProgressReporter reporter = null);

        /// <summary>
        /// Pause/Continue if paused - starting Process 
        /// </summary>
        void PauseContinueProc( CancellationToken cancelToken
                              , ProgressReporter reporter = null);// 
         


    }
}

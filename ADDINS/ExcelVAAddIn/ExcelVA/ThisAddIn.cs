﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using ExcelVA.Service;
using ExcelVA.XL;

namespace ExcelVA
{
    public partial class ThisAddIn
    {

        VAService Service 
        { get;   set; }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //STARTING SERVICE AND LOAD BS ELEMENTS
            Service = VAService.Current;
            Service.LoadBSElements();
            
            ExcelApp.SetExcelApp(Globals.ThisAddIn.Application );
           
            //Excel.Application _ExcelApp = Globals.ThisAddIn.Application;
            //Excel.Worksheet _Excelsheet = (Excel.Worksheet)ExcelApp.ActiveWorkbook.ActiveSheet
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            Service.Dispose();
        }

        #region Код, автоматически созданный VSTO

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}

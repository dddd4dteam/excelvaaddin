﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Config
{

    /// <summary>
    /// 1 - Name of last saved-Worked-opened file- to load auto on next open
    /// </summary>   
    public class LastSessionCfg
    {

        /// <summary>
        /// Last saved-Worked-opened file- to load auto on next open
        /// Only name not full path.
        /// </summary>
       
        public string LastWorkedFile
        { get; set; }

    }
}

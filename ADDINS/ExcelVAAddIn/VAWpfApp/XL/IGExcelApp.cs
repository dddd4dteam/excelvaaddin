﻿using Infragistics.Documents.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ExcelVA.XL
{
    /// <summary>
    /// Infragistics Excel methods to work with Exce files
    /// </summary>
    public class IGXL
    {
        /*
         
        private WorksheetCell _selectedCell= null;
        private List<string> _cellFormats=null;

             */

        public static Workbook CurrentWBook
        { get; private set; }

        public static Worksheet CurrentSheet
        { get; private set; }


        public static void LoadWorkBook(string excelFIlePath)
        {
            if (!File.Exists(excelFIlePath))
            {
                throw new FileNotFoundException(
                  "FILE NOT FOUND:", excelFIlePath);
            }

            var fileInf = new FileInfo(excelFIlePath);
            //Load the Excel file.
            using (Stream stream = fileInf.OpenRead() )  //OpenRead this.GetType().Assembly.GetManifestResourceStream(resourceName))
            {
                CurrentWBook  = Workbook.Load(stream);
            }
            fileInf = null;

        }

        public static void LoadWorkSheet(string sheetKey)
        {
            if (!CurrentWBook.Worksheets.Exists(sheetKey))
            {
                throw new InvalidOperationException(
                    "Can't fild SHeet with Name " + sheetKey);
            }      
          CurrentSheet =  CurrentWBook.Worksheets[sheetKey];
        }

        public static void CleanAll()
        {
            CurrentSheet = null;
            CurrentWBook = null;
            //GC.Collect();
        }

        public static WorksheetCell GetCell(string cellAdress)
        {
            var cell = CurrentSheet.GetCell( cellAdress);
            return cell;            
        }
        
        public static string GetCellText( string cellAdress)
        {
            var cell= CurrentSheet.GetCell(cellAdress);            
            return cell.GetText(); 
        }

        public static object GetCellValue(string cellAdress)
        {
            var cell = CurrentSheet.GetCell(cellAdress);
            return cell.Value;
        }


        public static WorksheetMergedCellsRegion GetCellAssocMergeRegion (string cellAdress)
        {
            var cell = CurrentSheet.GetCell(cellAdress);
            return cell.AssociatedMergedCellsRegion;

        }




    }
}

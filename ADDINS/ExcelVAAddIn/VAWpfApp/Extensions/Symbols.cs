﻿namespace ExcelVA
{
    public static class Symbols
    {
        #region ------------ ENGLISH SYMBOLS -----------

        public const string ABCEn = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string abcEn = "abcdefghijklmnopqrstuvwxyz";



        /// <summary>
        /// First Letter in English ABC : A ; Upper register
        /// </summary>
        public const char FLEn = 'A';
        /// <summary>
        /// First Letter in English ABC : a ; Lower register
        /// </summary>
        public const char flEn = 'a';

        /// <summary>
        /// Space Letter  ' '
        /// </summary>
        public const char SP = ' ';


        /// <summary>
        /// Last Letter  in  English ABC :  Z  ; Upper register
        /// </summary>
        public const char LLEn = 'Z';
        /// <summary>
        /// Last Letter  in  English ABC :  z  ; Lower register
        /// </summary>
        public const char llEn = 'z';


        #endregion ------------ ENGLISH SYMBOLS -----------

        #region ------------ RUSSIAN SYMBOLS -----------

        public const string ABCRu = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЬЭЮЯ";
        public const string abcRu = "абвгдеёжзийклмнопрстуфхцчшщъьэюя";


        #endregion ------------ RUSSIAN SYMBOLS -----------

        /// <summary>
        /// Excel Address-  Cell.Sheet[!]Cell.ColRow Delimeter
        /// </summary>
        public const string ShDm = "!";
        
        /// <summary>
        /// Excel Range CellStart[:]CellEnd Delimeter
        /// </summary>
        public const string RgDm = ":";

        /// <summary>
        /// Default Character for Char[] symbols
        /// </summary>
        public const char DC = '\0';

        // 10 chars
        public const string Digits = "0123456789";
              

    }
}

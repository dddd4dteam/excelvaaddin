﻿using ExcelVA;
using ExcelVA.XL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VAWpfApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        static void FileOUT(string messagelines)
        {
            var path = "c:\\Temp\\testout.txt";

            File.AppendAllText(path, messagelines + Environment.NewLine);

        }


        static void Test1()
        {
            try
            {
                //Load File  
                var path = "d:\\EXCEL\\TEST\\BookTest.xlsm";
                IGXL.LoadWorkBook(path);
                IGXL.LoadWorkSheet("VerifyConfig");

                // read cells 
                for (int i = 2; i < 9; i++)
                {
                    var adres = "B" + i.ToString();
                    var cell = IGXL.GetCell(adres);
                    var text = cell.GetText();

                    FileOUT(" Cell addres is [{0}]   Value is [{1}] \n"
                       .Fmt(adres, text));

                }

                IGXL.CleanAll();
            }
            catch (System.Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        static void Test2()
        {
            try
            {
                //Load File  

                var path = "d:\\EXCEL\\TEST\\BookTest.xlsm";
                // Получение полной книги ...
             

                IGXL.LoadWorkBook(path);
                IGXL.LoadWorkSheet("VerifyConfig");

                // read cells 
                for (int i = 2; i < 9; i++)
                {
                    var adres = "B" + i.ToString();
                    var cell = IGXL.GetCell(adres);
                    var text = cell.GetText();

                    FileOUT(" Cell addres is [{0}]   Value is [{1}] \n"
                       .Fmt(adres, text));

                }

                IGXL.CleanAll();
            }
            catch (System.Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}

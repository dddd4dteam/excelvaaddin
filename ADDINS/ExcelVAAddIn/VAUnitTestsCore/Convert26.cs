﻿using ExcelVA;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExcelVA.XL
{
    public class Convert26
    {
        public const int MAX_BIT = 32;
        public const int Base = 26;

        const int maxSymbols = 5;

        public static readonly Dictionary<char, int> AbcInd =
              new Dictionary<char, int>()
              {    {Symbols.FLEn, 1 },{'B', 2 },{'C', 3 }
                   ,{'D', 4 },{'E', 5 },{'F', 6 }
                   ,{'G', 7 },{'H', 8 },{'I', 9 }
                   ,{'J', 10 },{'K', 11 },{'L', 12 }
                   ,{'M', 13 },{'N', 14 },{'O', 15 }
                   ,{'P', 16 },{'Q', 17 },{'R', 18 }
                   ,{'S', 19 },{'T', 20 },{'U', 21 }
                   ,{'V', 22 },{'W', 23 },{'X', 24 }
                   ,{'Y', 25 },{ Symbols.LLEn, 26 }
              };

        public static readonly Dictionary<int, char> IndAbc =
             new Dictionary<int, char>()
             {    { 1,Symbols.FLEn },{ 2,'B' },{ 3,'C' }
                   ,{ 4,'D' },{ 5,'E' },{ 6,'F' }
                   ,{ 7,'G' },{ 8,'H' },{ 9,'I' }
                   ,{10,'J'},{11,'K'},{12,'L'}
                   ,{13,'M'},{14,'N'},{15,'O'}
                   ,{16,'P'},{17,'Q'},{18,'R'}
                   ,{19,'S'},{20,'T'},{21,'U'}
                   ,{22,'V'},{23,'W'},{24,'X'}
                   ,{25,'Y'},{26,Symbols.LLEn}
             };


        public static string IndexToABC(int colIndex)
        {
            int div = colIndex;
            string colLetter = String.Empty;
            int mod = 0;

            while (div > 0)
            {
                mod = (div - 1) % Base;
                colLetter = (char)(65 + mod) + colLetter;
                div = (int)((div - mod) / Base);
            }
            return colLetter;
        }


        public static int ABCToIndex(string columnLetter)
        {
            columnLetter = columnLetter.ToUpper();
            int sum = 0;

            for (int i = 0; i < columnLetter.Length; i++)
            {
                sum *= Base;
                sum += (columnLetter[i] - 'A' + 1);
            }
            return sum;
        }



        static int ABCToIndex2(string column)
        {
            var pow = 0;
            int idx = 0;
            for (int i = column.Length - 1; i >= 0; i--)
            {
                if (column[i] != Symbols.DC)
                {
                    idx += AbcInd[column[i]] * ((int)Math.Pow(Base, pow));
                    pow++;
                }
                else return idx;
            }
            return idx;
        }


        public static string Next2(string letter)
        {
            var nVal = letter.ToArrayRt(letter.Length + 1);
            var len = nVal.Length;            

            for (int i = len - 1; i >= 0; i--)
            {
                if (nVal[i] == Symbols.DC) { break; }

                if (nVal[i] != Symbols.LLEn)
                {
                    nVal[i] = IndAbc[AbcInd[nVal[i]] + 1];
                    break;
                }
                else if (nVal[i] == Symbols.LLEn)
                {
                    nVal[i] = Symbols.FLEn;
                    if ((i - 1) >= 0 && nVal[i - 1] == Symbols.DC)
                    {
                        nVal[i - 1] = Symbols.FLEn;
                        break;
                    }
                    continue;
                }
            }

            var nCol = (new string(nVal))
                        .TrimStart(Symbols.DC);
            return nCol;
        }
        
        public static string Prev2(string letter)
        {
            var nVal = letter.ToArrayRt(letter.Length);
            var len = nVal.Length;           

            for (int i = len - 1; i >= 0; i--)
            {
                if (nVal[i] == Symbols.DC) { break; }

                if (nVal[i] != Symbols.FLEn)
                {
                    nVal[i] = IndAbc[AbcInd[nVal[i]] - 1];
                    break;
                }
                else if (nVal[i] == Symbols.FLEn)
                {
                    if ((i - 1) >= 0
                        && nVal[i - 1] != Symbols.DC
                       )
                    {
                        nVal[i] = Symbols.LLEn;
                        continue;
                    }
                    else break;
                }
            }

            var nCol = (new string(nVal))
                        .TrimStart(Symbols.DC);
            return nCol;
        }



    }
}

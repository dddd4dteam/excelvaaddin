﻿using System.Linq;

namespace ExcelVA
{
    public static class SymbolsExtensions
    {
        
       public  static bool IsABCEnLetter( this char symbol)
        {
            return Symbols.ABCEn.Contains(symbol);
        }

        public static bool IsabcEnLetter(this char symbol)
        {
            return Symbols.abcEn.Contains(symbol);
        }

        public static bool IsEnLetter(this char symbol)
        {
            return  (  IsABCEnLetter(symbol) 
                    || IsabcEnLetter(symbol));
        }





        public static bool IsDigit(this char symbol)
        {
            return  Symbols.Digits.Contains(symbol);
        }

        public static char[] CloneChars(this char[] symbols)
        {
            var clone = new char[symbols.Length];
            for (int i = 0; i < symbols.Length; i++)
            {
                clone[i] = symbols[i];
            }

            return clone;
        }




        public static char[] ToArrayRt(this string value, int length)
        {
            var arrr = new char[length];
            var dist = length - value.Length;

            for (int i = value.Length - 1; i >= 0; i--)
            {
                arrr[i + dist] = value[i];
            }
            return arrr;
        }

    }
}

﻿// Decompiled with JetBrains decompiler
// Type: Infragistics.Documents.Excel.WorksheetCell
// Assembly: InfragisticsWPF4.Documents.Excel.v15.1, Version=15.1.20151.2251, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb
// MVID: 66F897AB-37B0-4960-9D5D-2F89E944E3B9
// Assembly location: D:\EXCEL\INFRAGISTOCS\InfragisticsWPF4.Documents.Excel.v15.1.dll

using Infragistics.Documents.Excel.FormulaUtilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;

namespace Infragistics.Documents.Excel
{
    public sealed class WorksheetCell : ICellFormatOwner, IComparable<WorksheetCell>, IFormattedStringOwner
    {
        internal static readonly WorksheetCell InvalidReference = new WorksheetCell((WorksheetRow)null, (short)-1);
        private static readonly HashSet<Type> _supportedCellTypes = new HashSet<Type>()
    {
      typeof (bool),
      typeof (byte),
      typeof (sbyte),
      typeof (short),
      typeof (ushort),
      typeof (int),
      typeof (uint),
      typeof (long),
      typeof (ulong),
      typeof (float),
      typeof (double),
      typeof (Decimal),
      typeof (char),
      typeof (string),
      typeof (Enum),
      typeof (DateTime),
      typeof (TimeSpan),
      typeof (StringBuilder),
      typeof (Guid),
      typeof (ErrorValue),
      typeof (FormattedString),
      typeof (StringElement),
      typeof (FormattedStringElement),
      typeof (SingleTargetFormula),
      typeof (ArrayInteriorFormula),
      typeof (DataTableInteriorFormula),
      typeof (DBNull)
    };
        private WorksheetCellFormatProxy cellFormatProxy;
        private int cellShiftHistoryVersion;

        private short columnIndex;

        private WorksheetRow row;

        internal WorksheetCell(WorksheetRow row, short columnIndex)
        {
            this.columnIndex = columnIndex;
            this.row = row;
            if (this.row == null)
                return;
            this.cellShiftHistoryVersion = this.row.Worksheet.CellShiftHistoryVersion;
        }

        WorksheetCellFormatProxy ICellFormatOwner.CellFormatInternal
        {
            get
            {
                return this.CellFormatInternal;
            }
        }

        bool ICellFormatOwner.HasCellFormat
        {
            get
            {
                return this.HasCellFormat;
            }
        }

        int IComparable<WorksheetCell>.CompareTo(WorksheetCell other)
        {
            if (object.ReferenceEquals((object)this, (object)other))
                return 0;
            if (other == (WorksheetCell)null)
                return -1;
            WorksheetRow row1 = this.Row;
            WorksheetRow row2 = other.Row;
            if (row1 == null && row2 == null)
                return 0;
            if (row1 == null)
                return 1;
            if (row2 == null)
                return -1;
            int num = row1.Index - row2.Index;
            if (num != 0)
                return num;
            return (int)this.columnIndex - (int)other.columnIndex;
        }

        void IFormattedStringOwner.OnUnformattedStringChanged(
          FormattedString sender)
        {
            this.Row?.UpdateFormattedStringElementOnCell(this.columnIndex, sender.Element);
        }

        public override bool Equals(object obj)
        {
            return ((IComparable<WorksheetCell>)this).CompareTo(obj as WorksheetCell) == 0;
        }

        public override int GetHashCode()
        {
            WorksheetRow row = this.Row;
            if (row == null)
                return 0;
            return row.GetHashCode() ^ this.columnIndex.GetHashCode();
        }

        public override string ToString()
        {
            CellReferenceMode cellReferenceMode = CellReferenceMode.A1;
            Worksheet worksheet = this.Worksheet;
            if (worksheet != null)
                cellReferenceMode = worksheet.CellReferenceMode;
            return this.ToString(cellReferenceMode, true);
        }

        public void ApplyFormula(string value)
        {
            WorksheetRow row = this.Row;
            if (row == null)
                throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
            row.ApplyCellFormula((int)this.columnIndex, value, new CellReferenceMode?());
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ClearComment()
        {
            WorksheetRow row = this.Row;
            if (row == null)
                throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
            row.SetCellCommentInternal(this.columnIndex, (WorksheetCellComment)null);
        }

        public Rect GetBoundsInTwips()
        {
            return this.GetBoundsInTwips(PositioningOptions.None);
        }

        public Rect GetBoundsInTwips(PositioningOptions options)
        {
            WorksheetRow row = this.Row;
            if (row == null)
                throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
            return row.GetCellBoundsInTwipsInternal(this.columnIndex, options);
        }

        public static string GetCellAddressString(
          WorksheetRow worksheetRow,
          int columnIndex,
          CellReferenceMode cellReferenceMode,
          bool includeWorksheetName)
        {
            return WorksheetCell.GetCellAddressString(worksheetRow, columnIndex, cellReferenceMode, includeWorksheetName, false, false);
        }

        public static string GetCellAddressString(
          WorksheetRow worksheetRow,
          int columnIndex,
          CellReferenceMode cellReferenceMode,
          bool includeWorksheetName,
          bool useRelativeColumn,
          bool useRelativeRow)
        {
            Worksheet worksheet = worksheetRow.Worksheet;
            return (includeWorksheetName ? Utilities.CreateReferenceString((string)null, worksheet.Name) : string.Empty) + CellAddress.GetCellReferenceString(worksheetRow.Index, columnIndex, worksheet.CurrentFormat, useRelativeRow, useRelativeColumn, worksheetRow.Index, (short)columnIndex, false, cellReferenceMode);
        }

        public WorksheetHyperlink GetHyperlink()
        {
            WorksheetRow row = this.Row;
            if (row == null)
                throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
            return row.GetCellHyperlink((int)this.columnIndex);
        }

        public string GetText()
        {
            return this.GetText(TextFormatMode.AsDisplayed);
        }

        public string GetText(TextFormatMode textFormatMode)
        {
            WorksheetRow row = this.Row;
            if (row == null)
                throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
            return row.GetCellTextInternal(this.columnIndex, textFormatMode);
        }

        public IWorksheetCellFormat GetResolvedCellFormat()
        {
            WorksheetRow row = this.Row;
            if (row == null)
                throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
            return row.GetResolvedCellFormat((int)this.columnIndex);
        }

        public static bool IsCellTypeSupported(Type cellType)
        {
            if (cellType == (Type)null)
                throw new ArgumentNullException(nameof(cellType));
            if (!WorksheetCell._supportedCellTypes.Contains(cellType))
                return cellType.IsEnum();
            return true;
        }

        public string ToString(CellReferenceMode cellReferenceMode, bool includeWorksheetName)
        {
            WorksheetRow row = this.Row;
            if (row == null)
                return ErrorValue.InvalidCellReference.ToString();
            return row.GetCellAddressString((int)this.columnIndex, cellReferenceMode, includeWorksheetName, false, false);
        }

        public string ToString(
          CellReferenceMode cellReferenceMode,
          bool includeWorksheetName,
          bool useRelativeColumn,
          bool useRelativeRow)
        {
            WorksheetRow row = this.Row;
            if (row == null)
                return ErrorValue.InvalidCellReference.ToString();
            return row.GetCellAddressString((int)this.columnIndex, cellReferenceMode, includeWorksheetName, useRelativeColumn, useRelativeRow);
        }

        public bool ValidateValue()
        {
            WorksheetRow row = this.Row;
            if (row == null)
                return true;
            return row.ValidateCellValue(this.ColumnIndex);
        }

        internal WorksheetRegion GetCachedRegion()
        {
            WorksheetRow row = this.Row;
            if (row == null || row.Worksheet == null)
                return (WorksheetRegion)null;
            return row.Worksheet.GetCachedRegion(row.Index, (int)this.columnIndex, row.Index, (int)this.columnIndex);
        }

        private void OnShiftedOffWorksheet()
        {
            this.row = (WorksheetRow)null;
            this.columnIndex = (short)-1;
        }

        private void VerifyCellAddress()
        {
            if (this.row == null)
                return;
            Worksheet worksheet = this.row.Worksheet;
            if (worksheet != null && worksheet.VerifyCellAddress(ref this.row, ref this.columnIndex, ref this.cellShiftHistoryVersion))
                return;
            this.OnShiftedOffWorksheet();
        }

        public WorksheetDataTable AssociatedDataTable
        {
            get
            {
                return this.Row?.GetCellAssociatedDataTableInternal(this.columnIndex);
            }
        }

        public WorksheetMergedCellsRegion AssociatedMergedCellsRegion
        {
            get
            {
                return this.Row?.GetCellAssociatedMergedCellsRegionInternal(this.columnIndex);
            }
        }

        public WorksheetTable AssociatedTable
        {
            get
            {
                return this.Row?.GetCellAssociatedTableInternal(this.columnIndex);
            }
        }

        public IWorksheetCellFormat CellFormat
        {
            get
            {
                return (IWorksheetCellFormat)this.CellFormatInternal;
            }
        }

        internal WorksheetCellFormatProxy CellFormatInternal
        {
            get
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    return (WorksheetCellFormatProxy)null;
                if (this.cellFormatProxy == null)
                    this.cellFormatProxy = row.GetCellFormatInternal(this.columnIndex);
                return this.cellFormatProxy;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool HasCellFormat
        {
            get
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    return false;
                return row.TryGetCellFormat(this.columnIndex) != null;
            }
        }

        public int ColumnIndex
        {
            get
            {
                return (int)this.ColumnIndexInternal;
            }
        }

        internal short ColumnIndexInternal
        {
            get
            {
                this.VerifyCellAddress();
                return this.columnIndex;
            }
        }

        public WorksheetCellComment Comment
        {
            get
            {
                return this.Row?.GetCellCommentInternal(this.columnIndex);
            }
            set
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
                row.SetCellCommentInternal(this.columnIndex, value);
            }
        }

        public DataValidationRule DataValidationRule
        {
            get
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    return (DataValidationRule)null;
                Worksheet worksheet = row.Worksheet;
                if (!worksheet.HasDataValidationRules)
                    return (DataValidationRule)null;
                return worksheet.DataValidationRules.FindRule(this);
            }
            set
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
                Worksheet worksheet = row.Worksheet;
                if (value == null)
                {
                    if (!worksheet.HasDataValidationRules)
                        return;
                    worksheet.DataValidationRules.Remove(this);
                }
                else
                    worksheet.DataValidationRules.Add(value, WorksheetReferenceCollection.CreateFromCell(this));
            }
        }

        public bool HasComment
        {
            get
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    return false;
                return row.GetCellCommentInternal(this.columnIndex) != null;
            }
        }

        public Formula Formula
        {
            get
            {
                return this.Row?.GetCellFormulaInternal(this.columnIndex);
            }
        }

        public int RowIndex
        {
            get
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    return -1;
                return row.Index;
            }
        }

        public object Value
        {
            get
            {
                return this.Row?.GetCellValueInternal(this.columnIndex);
            }
            set
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    throw new InvalidOperationException(Utilities.GetString("LE_ArgumentException_CellShiftedOffWorksheet"));
                row.SetCellValue((int)this.columnIndex, value);
            }
        }

        public Worksheet Worksheet
        {
            get
            {
                return this.Row?.Worksheet;
            }
        }

        internal WorksheetCellAddress Address
        {
            get
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    return WorksheetCellAddress.InvalidReference;
                return new WorksheetCellAddress(row.Index, this.columnIndex);
            }
        }

        internal WorksheetRegionAddress RegionAddress
        {
            get
            {
                WorksheetRow row = this.Row;
                if (row == null)
                    return WorksheetRegionAddress.InvalidReference;
                return new WorksheetRegionAddress(row.Index, row.Index, this.columnIndex, this.columnIndex);
            }
        }

        internal WorksheetRow Row
        {
            get
            {
                this.VerifyCellAddress();
                return this.row;
            }
        }

        public static bool operator ==(WorksheetCell a, WorksheetCell b)
        {
            if (object.ReferenceEquals((object)a, (object)b))
                return true;
            bool flag1 = (object)a == null;
            bool flag2 = (object)b == null;
            if (flag1 && flag2)
                return true;
            if (flag1 || flag2)
                return false;
            a.VerifyCellAddress();
            b.VerifyCellAddress();
            if (a.row == b.row)
                return (int)a.columnIndex == (int)b.columnIndex;
            return false;
        }

        public static bool operator !=(WorksheetCell a, WorksheetCell b)
        {
            return !(a == b);
        }
    }
}

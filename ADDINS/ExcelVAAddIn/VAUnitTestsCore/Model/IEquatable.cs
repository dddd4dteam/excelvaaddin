﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  System
{
    public interface IEquatable
    {
        bool EqualsTo(object otherItem);
    }
}

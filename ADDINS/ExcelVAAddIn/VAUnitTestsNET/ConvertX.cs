﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VATests
{
    public class ConvertX
    {
        public const int MAX_BIT = 32;

        public static string DecToBase(int num_value, int base_value)
        {
            var max_bit = MAX_BIT;
            var dec_base = 10;
            var hexchars = new[] { 'A', 'B', 'C', 'D', 'E', 'F' };
            var result = string.Empty;
            var result_array = new int[MAX_BIT];

            for (/* nothing */; num_value > 0; num_value /= base_value)
            {
                int i = num_value % base_value;
                result_array[--max_bit] = i;
            }

            for (int i = 0; i < result_array.Length; i++)
            {
                if (result_array[i] >= dec_base)
                {
                    result += hexchars[(int)result_array[i] % dec_base].ToString();
                }
                else
                {
                    result += result_array[i].ToString();
                }
            }

            result = result.TrimStart(new char[] { '0' });
            return result;
        }

    }
}

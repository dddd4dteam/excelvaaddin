﻿using ExcelVA;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ExcelVA.Analise.Model
{

    [JsonObject]
    public class Cell: IEquatable, IEquatable<Cell>
    {
        public Cell() { }

        public Cell(string address)
        {
            Adres = new Address(address);            
        }

        [JsonProperty]
        public Address Adres
        { get; set; }


        [JsonProperty]
        public string Text
        { get; set; }


        [JsonProperty]   //MERGED,COLOR,BORDER ...  
        public Dictionary<string, string> Format
        { get; set; } = new Dictionary<string, string>();

 

        public Cell NextInCol()
        {
            return new Cell(
                Adres.Column.Val + (Adres.Row++).S()
                );
        }

        public Cell NextInRow()
        {
            return new Cell(
                Adres.Column.Next().Val + Adres.RowFmt
                );
        }


        public Cell PrevInCol()
        {
            return new Cell(
                Adres.Column.Val + (Adres.Row--).S()
                );
        }

        public Cell PrevInRow()
        {
            return new Cell(
                Adres.Column.Prev().Val + Adres.RowFmt
                );
        }



        public void GetValueIG()
        {

        }

        public void GetValue()
        {

        }

        public bool Equals(Cell otherCell)
        {
            //COMPARE ADDRESS
            if ( Adres.Equals(otherCell.Adres) == false)
            {   return false;
            }

            if (Text != otherCell.Text)
            { return false;
            }

            foreach (var item in Format)
            {
                if (   !otherCell.Format.ContainsKey(item.Key) 
                    ||  otherCell.Format[item.Key] != item.Value 
                   )
                {
                    return false;
                }
            }             

            return true;
        }


        public bool EqualsTo(object other)
        {
            return Equals(other as Cell);
        }


    }


}


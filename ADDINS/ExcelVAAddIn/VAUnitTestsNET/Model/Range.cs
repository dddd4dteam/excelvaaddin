﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Analise.Model
{

    [JsonObject]
    public class Range: IEquatable, IEquatable<Range>
    {
        public Range() { }

        public Range(string rangeAddres)
        {
            Address = rangeAddres;
            ParseAddress();
        }

        [JsonProperty]
        public string Address
        { get; set; }


        [JsonProperty]
        public Cell From
        { get; set; }


        [JsonProperty]
        public Cell To
        { get; set; }


        public bool Equals(Range otherRange)
        {
            if (Address != otherRange.Address)
            {
                return false;
            }

            if (  From.Equals(otherRange.From) == false )
            {
                return false;
            }

            if ( To.Equals(otherRange.To) == false)
            {
                return false;
            }

            return true;
        }


        public bool EqualsTo(object otherRange)
        {
            return Equals(otherRange as Range);
        }

        public void ParseAddress()
        {
            //CELL FROM   CELL TO            
            // Exmpl: [D8:G20] 

            if (Address.IsNotNull())
            {
                var cells = Address.SplitNoEntries(Symbols.RgDm);
                if (cells.Count == 2)
                {
                    From = new Cell(cells[0]);
                    To = new Cell(cells[1]);
                }
            }
        }


    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelVA.Analise.Model
{

    [JsonObject]
    public  class CellInfo: IEquatable, IEquatable<CellInfo>
    {
        public CellInfo() { }


        [JsonProperty]
        public string Adres
        { get; set; }


        [JsonProperty]
        public string Text
        { get; set; }


        [JsonProperty]
        public Dictionary<string, string> Format
        { get; set; } = new Dictionary<string, string>();


        public bool Equals(CellInfo otherCell)
        {
            //COMPARE ADDRESS
            if (Adres != otherCell.Adres)
            {
                return false;
            }

            if (Text != otherCell.Text)
            {
                return false;
            }

            foreach (var item in Format)
            {
                if (!otherCell.Format.ContainsKey(item.Key)
                    || otherCell.Format[item.Key] != item.Value
                   )
                {
                    return false;
                }
            }

            return true;
        }

        public bool EqualsTo(object otherCell)
        {
            return Equals(otherCell as CellInfo);
        }
    }
}

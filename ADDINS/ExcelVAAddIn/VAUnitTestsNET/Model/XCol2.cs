﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using ExcelVA;
using ExcelVA.XL;

namespace ExcelVA.Analise.Model
{

    [JsonObject]
    public struct XCol : ICloneable, IEquatable, IEquatable<XCol>

    {
        #region --------- CTORS----------

       // public XCol2() { } //only for serialization

        public XCol(string column)
        {
            Val = column;
            Index = Convert26.ABCToIndex(Val);
        }

        public XCol(int index)
        {
           Index = index;
           Val = Convert26.IndexToABC(index);
        }


        XCol(string column, int index)
        {
            Val = column;
            Index = index;
        }

        #endregion --------- CTORS----------

         
        #region ----------- PROPS------------

        /// <summary>
        /// Column  value in char[] of 5 symbols length
        /// </summary>
        [JsonProperty]
        public string Val
        { get; set; }

        /// <summary>
        /// Char value converted to integer
        /// </summary>
        [JsonProperty]
        public int Index
        { get; set; }


        public int Len
        {
            get
            {
                return Val.Length;
            }
        }

        public char Last
        {
            get
            {
                return Val[Len - 1];
            }

        }


        #endregion ----------- PROPS------------

     

        #region -----------  NEXT ---------

        public XCol Next()
        {
            var nCol = Convert26.Next2(Val);            
            var nIndex = Index++;          

            return new XCol(nCol, nIndex);
        }


        public XCol Next(int plusStep)
        {
            var nIndex = Index + plusStep;
            var nCol = Convert26.IndexToABC(nIndex);
             
            return new XCol(nCol, nIndex);
        }

        #endregion -----------  NEXT ---------


        #region -----------  PREV ---------


        public XCol Prev()
        {
            var nCol = Convert26.Prev2(Val);
            var nIndex = Index--;

            return new XCol(nCol, nIndex); 
        }


        public XCol Prev(int minusStep)
        {
            var nIndex = Index - minusStep;
            var nCol = Convert26.IndexToABC(nIndex);

            return new XCol(nCol, nIndex);
        }


        #endregion ----------- PREV ---------





        #region ----------- ICloneable --------

        public XCol Clone()
        {
            var nCol = new XCol
            {
                Val = Val.Clone() as string,
                Index = Index
            };

            return nCol;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public bool Equals(XCol otherCol)
        {
            if (Val != otherCol.Val)
            { return false; 
            }

            return true;
        }

        public bool EqualsTo(object otherItem)
        {
            return Equals( (XCol)otherItem);
        }

        #endregion ----------- ICloneable --------


    }
}

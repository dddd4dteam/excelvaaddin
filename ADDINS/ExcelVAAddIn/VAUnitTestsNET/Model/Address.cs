﻿using System;
using System.Collections.Generic;
using ExcelVA;
using Newtonsoft.Json;

namespace ExcelVA.Analise.Model
{

    [JsonObject]
    public class Address : IEquatable, IEquatable<Address>
    {
        public Address()
        { }

        public Address(string adress)
        {
            Value = adress;

            string ColmnRowStr = null;
            // Contains Sheet Name
            if (adress.Contains(Symbols.ShDm ))
            {
                var sheetNames = adress.SplitNoEntries("!");
                Sheet = sheetNames[0];
                ColmnRowStr = sheetNames[1];
            }
            else //address of current list
            {
                ColmnRowStr = adress;
            }
            
            var colRow = ToColRow(ColmnRowStr);
            Column =  colRow.Key;
            Row = colRow.Value;
        }

         Address(string sheet, XCol col, int row)
        {
            Sheet = sheet;
            Column = col;
            Row = row;
            if (Sheet.IsNotNull())
            {
                Value = Sheet + Symbols.ShDm + Column.Val + row.S();
            }
            else Value = Column.Val + row.S();

        }

        static KeyValuePair<XCol, int> ToColRow(string address)
        {
            string colmn = "";
            string row = "";
            int rowId = -1;
            for (int i = 0; i < address.Length; i++)
            {
                if (address[i].IsABCEnLetter())
                {
                    colmn += address[i];
                }
                else
                {
                    row += address[i];
                }
            }
            rowId = int.Parse(row);
            var nCol = new XCol(colmn );
            return new KeyValuePair<XCol, int>(nCol, rowId);
        }


        [JsonProperty]
        public string Value
        { get; set; }


        [JsonProperty]
        public string Sheet
        { get; set; }

        [JsonProperty]
        public XCol Column
        { get; set; }

        [JsonProperty]
        public int Row
        { get; set; }


        string rowFmt = null;
        public string RowFmt
        {
          get {
                if (rowFmt.IsNull())
                {
                    rowFmt = Row.S();
                }
                return rowFmt;
            }
        }

        

        public Address NextInCol()
        {
            return new Address(Sheet
                            , Column.Clone()
                            , (Row + 1) );
        }

        public Address NextInCol(int plusStep)
        {
            return new Address(Sheet
                              , Column.Clone()
                              , (Row + plusStep) );
        }


        public Address NextInRow()
        {
            return new Address(Sheet
                              , Column.Next()
                              , Row );
        }

        public Address NextInRow(int plusStep)
        {
            return new Address(Sheet
                              , Column.Next(plusStep)
                              , Row );
        }


        public Address PrevInCol()
        {
            return new Address(Sheet
                            , Column.Clone()
                            , (Row - 1));
        }

        public Address PrevInCol(int plusStep)
        {
            return new Address(Sheet
                              , Column.Clone()
                              , (Row - plusStep));
        }





        public Address PrevInRow()
        {
            return new Address(Sheet
                              , Column.Prev()
                              , Row);
        }

        public Address PrevInRow(int plusStep)
        {
            return new Address(Sheet
                              , Column.Prev(plusStep)
                              , Row);
        }


        public bool Equals(Address other)
        {
            return (Value == other.Value);
        }


        public bool EqualsTo(object other)
        {
            return Equals( other as Address);
        }

    }
}

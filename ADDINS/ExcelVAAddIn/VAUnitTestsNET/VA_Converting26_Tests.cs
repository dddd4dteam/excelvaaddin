//using ExcelVA.Analise;
using ExcelVA.Analise.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using ExcelVA;
using System.IO;
using System.Collections.Generic;
using ExcelVA.XL;

namespace VATests
{
    [TestClass]
    public class VA_Converting26_Tests
    {
 
        //Cell Addresses testing
        

        static void FileOUT(string messagelines)
        {
            var path = "c:\\Temp\\testout.txt";

            File.AppendAllText(path, messagelines + Environment.NewLine);

        }


        [TestMethod]
        public void Test_Convert26()
        {
            List<string> letters = new List<string>();

            for (int i = 1; i < 400; i += 20)
            {
                var val = Convert26.IndexToABC(i);
                FileOUT("  Convert26 Index is [{0}] val is [{1}] \n"
                  .Fmt(i.S(), val));
                letters.Add(val);
            }

            for (int i = 0; i < letters.Count; i++)
            {
                var val = Convert26.ABCToIndex(letters[i]);
                FileOUT("  Convert26 Index is [{0}] val is [{1}] \n"
                  .Fmt(i.S(), val.S()));
            }

        }

         


        [TestMethod]
        public void Test_Cells()
        {
            var cl = new Cell("B17");
            var row = cl.Adres.Row;
            var colmn = cl.Adres.Column;
            var nextCol = colmn.Next();
            var clNRow = cl.NextInRow();
            var clNCol = cl.NextInCol();

            var result = "Cell address[{0}]"
                .Fmt(clNRow.Adres.Value);

            result = "Cell address[{0}]"
                .Fmt(clNCol.Adres.Value);


        }



        [TestMethod]
        public void Test_XCol2()
        {
            var col1 = new XCol("A");

            var nextCol = col1.Next();
            for (int i = 2; i < 100; i++)
            {
                FileOUT(" Column Index is [{0}] val is [{1}] \n"
                   .Fmt(i.S(), nextCol.Val));

                nextCol = nextCol.Next();                                
            }
          

        }

       

        [TestMethod]
        public void Test_Addresses_NextInRow()
        {
            var asr1 = new Address("A1");

            var nextAdr = asr1.NextInRow();
            for (int i = 2; i < 100; i++)
            {
                FileOUT(" Adress Index is [{0}] val is [{1}] \n"
                   .Fmt(i.S(), nextAdr.Value));

                nextAdr = nextAdr.NextInRow();
            }
        }


        [TestMethod]
        public void Test_Addresses_NextInCol()
        {
            var asr1 = new Address("A1");

            var nextAdr = asr1.NextInCol();
            for (int i = 2; i < 100; i++)
            {
                FileOUT(" Adress Index is [{0}] val is [{1}] \n"
                   .Fmt(i.S(), nextAdr.Value));

                nextAdr = nextAdr.NextInCol();
            }
        }


       



    }
}
